#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2023
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2


import requests

access_token = "must be given from the hifis idp"
resp = requests.get(
    "http://localhost:8080/srv/api/hifis/sync-groups/",
    headers={
        "Authorization": access_token,
    },
)
print(resp)
