#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2023
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2


import requests

IDL_TOKEN = "must be equal to the IDL_TOKEN env variable of the idl service"
resp = requests.get(
    "http://localhost:8080/srv/api/idl/permission-groups/",
    headers={
        #
        "Authorization": f"Bearer {IDL_TOKEN}",
    },
)
