<!--
SPDX-FileCopyrightText: 2023
- Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
- Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)

SPDX-License-Identifier: EUPL-1.2
-->


# GFZ-IDL

This project contains the GFZ implementation of the IDL specification:

https://codebase.helmholtz.cloud/hub-terra/institute-decoupling-layer/idl-api/-/blob/master/openapi.json

It is used to allow an unified access to user information within the sensor management system,
without dealing with centre specific issues.

## Features

- Database to store & query group information based on the IDL
- Option to extract group data from HIFIS Virtual Organizations
- Admin interface to check the groups

## Server

It currently runs on [https://rz-vm64.gfz-potsdam.de/idl](https://rz-vm64.gfz-potsdam.de/idl/admin/)
This is part of the staging system of the gfz sensor management system instance.

It is only accessible from within the GFZ intranet.
## Feedback

If you have problems or ideas for improvement, please create an issue in the
[project board](https://codebase.helmholtz.cloud/hub-terra/sms/gfz-idl/-/issues)

## Authors

- Nils Brinckmann

## Architecture

The basic architecture remains of the following things:

- a django backend with api endpoints
- a postgres database to store the user & group information
- a nginx to deliver static pages

## Contributing

If you want to contribute, please contact one of the following persons:

- nils.brinckmann@gfz-potsdam.de

Please note that this project have strong usage of linters (flake8) & code
formatters (black).

## Installation

### Start development environment

The easiest way is to use `docker` and `docker-compose`.

```bash
docker-compose -f docker-compose-dev.yml up --build
```

You may open the admin interface of the django application:

[http://localhost:8080/srv/](http://localhost:8080/srv/admin)

## Run the tests

To run the tests start the docker compose as mentioned in the installation process:

```bash
docker-compose -f docker-compose-dev.yml up -d
```

Then start the shell in the docker-container:

```bash
docker exec -ti gfz-idl_backend_1 bash
```

And execute the django unittests:

```bash
cd project
python3 manage.py test
```

## Pages & API Reference

### Pages

The only non api pages that the GFZ-IDL provides is the django admin interface:
[http://localhost:8080/srv/admin](http://localhost:8080/srv/admin)

### Backend

Please note: All the backend endpoints require a token for the authentication.

#### Auth

In order to restrict access to personal information to an appropriate degree,
we need a token to be sent with the get request.

The accepted value for the auth is set with the `IDL_TOKEN` env variable.

```python
import requests

IDL_TOKEN = "must be equal to the IDL_TOKEN env variable of the idl service"
resp = requests.get(
    "http://localhost:8080/srv/api/idl/permission-groups/",
    headers={
        #
        "Authorization": f"Bearer {IDL_TOKEN}",
    },
)
```

You can find this script in `assistance/send_get_permission_groups_request.py`.

#### Exposed entries

All the database elements have an `expose` flag.
Only entries with those set to true are used in the IDL backend routes. 

#### Get all the permission groups

```text
GET http://localhost:8080/srv/api/idl/permission-groups/
```

#### Get one specific permission group

```text
GET http://localhost:8080/srv/api/idl/permission-groups/{id}
```

#### Get all the user accounts

```text
GET http://localhost:8080/srv/api/idl/user-accounts/
```

#### Get one specific permission group

```text
GET http://localhost:8080/srv/api/idl/user-accounts/{id}
```


#### Sync with the HIFIS VO groups

It is also possible to extract groups from HIFIS Virtual Organisations.

This is a going to be complex quickly, so we may take some longer explanation.

##### How to trigger the sync

You can do so by sending a request with the access token that you can get from the HIFIS IDP
server. This is an example on how you could do it in python:

```python
import requests

access_token = "must be given from the hifis idp"
resp = requests.get(
    "http://localhost:8080/srv/api/hifis/sync-groups/",
    headers={
        "Authorization": access_token,
    },
)
print(resp)
```

You can find that script under `assistance/send_sync_request.py`.

A successful synchronization will just return an 204 response without further content.

##### What happens exactly

###### Extraction of the user information

1. Based on the value of the `OIDC_WELL_KNOWN_URL` variable, the backend will query the configuration for an
   identity provider. In this example we will use `https://login-dev.helmholtz.de/oauth2/.well-known/openid-configuration`.
   We make a get request to get some more information about the IDP.
   
2. In this response there is the entry for `userinfo_endpoint`. It is `https://login-dev.helmholtz.de/oauth2/userinfo`.

3. We will send the access token that we got with the initial request to the userinfo endpoint.
   As a result we get some json response with data about the user.
   
4. We will extract the entries for `eduperson_principal_name` and `eduperson_entitlement`.
   The principal name will look like a username ala `<name>@<institute>.de`.
   The entitlements are a longer list with entries that look like `urn:geant:helmholtz.de:group:Moses:Permafrost#login.helmholtz.de` or `urn:geant:helmholtz.de:group:dummy:sec52.gfz-sms-admin#idp.gfz-potsdam.de`.
   
###### Parsing of the groups & roles

1. The backend will look up the variables for `VO_MEMBER_CONVENTIONS` and `VO_ADMIN_CONVENTIONS`.
   They may look like `*:gfz-sms-member` or `:gfz:sms-admin`.
   Those are conventions to handle parts of the entitlements as roles.
   You can read more about it [here](https://hifis.net/doc/service-integration/roles/roles-datahub/).
   The conventions here are build from two parts (splitted by `:`). The first part is a pattern
   for specific virtual organisations. The second part is the pattern for the role.
   Both follow unix shell semantic, so an `*` is a wildcard for any kind of text.
   The backend supports multiple conventions (splitted by `,`), so it may look like `specific_vo:admin,*:gfz-sms-member`.
   The first match for a VO in this list defines with conventions are used, so place the more specific one before the wildcards.
   
2. The backend will parse the entitlements & split them up into the role part as well as the name of the
   groups in the virtual organisation. So it will parse 
   `urn:geant:helmholtz.de:group:dummy:sec52.gfz-sms-admin#idp.gfz-potsdam.de` into
   a virtual organisation `dummy`, a group `sec52` and the `gfz-sms-admin` role.
   
3. The backend then tests the conventions. In our example it can match the virtual organisation `dummy` with the
   wildcard `*` and will then see that the role part of the conventions matches the parsed role as well. With 
   `gfz-sms-admin` they are identical. So we know that the user is an admin in the `dummy:sec52` group - we put
   the virtual organisation and the group name together to make them distinguish considering multiple virtual organisations.
   
4. The backend does this for all entitlements - and collects all the groups that the user is admin or member.

###### Synchronization itself

1. With all the information from the parsed & interpreted entitlement data, we can make change to the database.
2. If the user doesn't exist so far, then we create the entry.
3. For every group we extracted and that isn't so far in the system, we will create them as permission group.
4. We add the user in its role (member or admin) for those groups.
5. The backend removes the user from groups in which she/he isn't member or admin anymore.

Please note: All the users & groups that are created or extended with this mechanism get a true expose flag,
so that they are included in the IDL routes without further adjustments.
   
##### All the bloody details

In case you want to see exactly what happens, then take a look at:

- `backend/project/app/hifis/openidconnect.py` to see the logic of the sync proces itself.
- `backend/project/app/tests/test_hifis/test_groupconventions.py` for the tests for the usage of the group & role conventions.

#### User for Django administration

To create an account for the Django administration website run the following:

```bash
$ docker exec -ti gfz-idl_backend_1 python3 project/manage.py createsuperuser
Username (leave blank to use 'root'): admin
Email address: yourmail@example.com
Password:
Password (again):
Superuser created successfully.
```

## Deployment

We aim to provide the deployment via CI/CD pipelines powered
by gitlab-ci and handled in docker containers.

You can take a look at the pipelines in [.gitlab-ci.yml](.gitlab-ci.yml)

Hint: You can run parts of the pipelines locally using
[gitlabci-local](https://pypi.org/project/gitlabci-local/)

## Environment Variables

Due to the usage of docker containers there are a lot of environment variables
to mention.

### backend

| Variable                | Description                                                                                                                                                                                             |
|-------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `DJANGO_DEBUG`          | specifies if django runs in the debug mode.                                                                                                                                                             |
| `DJANGO_DB_{XXX}`       | Connection parameters for the database. We can set the engine, name, user, password, host & port.                                                                                                       |
| `DJANGO_BASE_PATH`      | Specifies the base path used. We currently use `srv/`, but we set it as we want.                                                                                                                        |
| `IDL_TOKEN`             | Token that is checked when working with the idl endpoints. Requests without it will be blocked.                                                                                                         |
| `OIDC_WELL_KNOWN_URL`   | Configuration URL of the IDP to extract information from for sync mechanism with HIFIS. If login-dev.helmholtz.de is used, then only the virtual organisations from the dev server are provided.        |
| `DJANGO_ALLOWED_HOSTS`  | Host names that django will allow to work with. Can be a space seperated list of names.                                                                                                                 |
| `DJANGO_CSRF_TRUSTED_ORIGINS`  | Specifies the list of trusted origins for unsafe requests (e.g., POST requests) when dealing with Cross-Site Request Forgery (CSRF) protection in Django.    |
| `VO_ADMIN_CONVENTIONS`  | A value of `*:gfz-sms-admin` will interpret every member of a `gfz-sms-admin` sub group as an admin for the upper group. The `*` here is shell pattern to match every VO. It can also be more specific. |
| `VO_MEMBER_CONVENTIONS` | Same mechanism as for the `VO_ADMIN_CONVENTIONS`, but for the member role.                                                                                                                              |

### backend-db

`POSTGRES_PASSWORD`
`POSTGRES_USER`
`POSTGRES_DB`

## FAQ

### Can I also use just the admin interface to manage my groups?

Yes it is perfectly possible.

You can open the backend/project/app/admin.py and replace it with the following content:
```python
from django.contrib import admin
from .models import PermissionGroup, UserAccount

admin.site.register(PermissionGroup)
admin.site.register(UserAccount)
```

After that you should be able to handle all the groups & user information with the django admin interface.

It makes sense to remove the synchronization endpoint for the HIFIS token, so that
this mechanism doesn't make any unexpected changes.

Please also note that the name of the user account should still be a name
that can be found from client systems. For example the sensor management
system backend will search for the `eduperson_principal_name` of the HIFIS token
data. It looks like `username@institute.org`.

For example: If your institute username is `user` and you
work for the `GFZ`
then you must use `user@gfz-potsdam.de` as username value in
the admin interface.
Using just `user` will not be sufficient.

## Lessons Learned

- Make sure that users have approved that their data is used in an application. It is important in regard to the
  german DSGVO (Datenschutzgrundverordnung).
- For the Helmholtz AAI usage & the entitlements the user accept that their entitlements are delivered to an application.
  So this is ok in regard to the DSGVO.
## Roadmap
...
## Acknowledgements
- [reamdme.so](https://readme.so/editor)
