#!/bin/bash

# SPDX-FileCopyrightText: 2021 - 2022
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2


set -e

cd `dirname $0`

/usr/local/bin/python3 project/manage.py migrate
/usr/local/bin/python3 project/manage.py runserver 0.0.0.0:8000
