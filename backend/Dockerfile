# SPDX-FileCopyrightText: 2021 - 2022
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2

FROM python:3.9-slim-buster

WORKDIR /usr/src/app

ENV DJANGO_DEBUG false
ENV DJANGO_DB_ENGINE django.db.backends.postgresql
ENV DJANGO_DB_NAME backend_db
ENV DJANGO_DB_USER backend_db
ENV DJANGO_DB_PASSWORD backend_db_password123456
ENV DJANGO_DB_HOST backend-db
ENV DJANGO_DB_PORT 5432

RUN DEBIAN_FRONTEND=noninteractive apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get upgrade -y && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y \
        binutils \
        cron \
        gcc \
        libldap2-dev \
        libsasl2-dev \
        rsyslog

COPY . .

RUN pip install -r requirements-wsgi.txt

CMD [ "/usr/src/app/start.sh" ]
