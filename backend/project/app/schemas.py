# SPDX-FileCopyrightText: 2021 - 2022
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""Schemas to be used in the views & viewsets."""


class PermissionGroupSchema:
    """Schema for the PermissionGroup model."""

    def __init__(self, only_exposed_users):
        """
        Init the object.

        With the only_exposed_users we can specify if we want to
        serialize non exposed users.
        """
        self.only_exposed_users = only_exposed_users

    def serialize_instance_to_dict(self, group):
        """Transform the group into a dict as output for views."""
        return {
            "id": str(group.id),
            "name": group.name,
            "description": group.description,
            "members": [str(u.id) for u in self._filter_users(group.members.all())],
            "admins": [str(u.id) for u in self._filter_users(group.admins.all())],
        }

    def _filter_users(self, user_queryset):
        """Filter the users depending on our only_exposed_users setting."""
        if self.only_exposed_users:
            return user_queryset.filter(expose=True)
        return user_queryset


class UserAccountSchema:
    """Schema for the UserAccount model."""

    def __init__(self, only_exposed_groups):
        """
        Init the object.

        With the only_exposed_groups we can specify if we want to
        serialize non exposed groups.
        """
        self.only_exposed_groups = only_exposed_groups

    def serialize_instance_to_dict(self, user):
        """Transform a user into a dict as output for views."""
        return {
            "id": str(user.id),
            "userName": user.username,
            "displayName": user.displayname,
            "administratedPermissionGroups": [
                str(g.id) for g in self._filter_groups(user.administrated_groups.all())
            ],
            "memberedPermissionGroups": [
                str(g.id) for g in self._filter_groups(user.membered_groups.all())
            ],
        }

    def _filter_groups(self, group_queryset):
        """Filter the groups depending on our only_exposed_groups setting."""
        if self.only_exposed_groups:
            return group_queryset.filter(expose=True)
        return group_queryset
