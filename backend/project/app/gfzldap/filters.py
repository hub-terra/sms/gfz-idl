# SPDX-FileCopyrightText: 2021
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""Various filter functions that can be used by the fields."""


def equals(field_value, comparsion_value):
    """Return true if both are equal."""
    return field_value == comparsion_value


def is_null_or_empty(field_value, comparsion_value):
    """
    Check if the value is null or empty.

    Depending on the comparsion_value the result differs.
    If the comparsion_value is True, it will return True
    for None or empty values.
    If the comparsion_value is False, it will return the
    oposite.
    """
    if not field_value:
        return comparsion_value
    return not comparsion_value


def one_of_the_values_equals(field_values, comparsion_value):
    """Return true if one of the field_values is equal to the comparsion_value."""
    return comparsion_value in field_values


def contains(field_value, comparsion_values):
    """Return true if the field_value is in the comparsion value list."""
    return field_value in comparsion_values


def list_contains_one(field_values, comparsion_values):
    """Return true of one of the field values is in the comparsion_values."""
    return any([field_value in comparsion_values for field_value in field_values])


def startswith(field_value, comparsion_value):
    """Return true if the string startswith the comparsion_value."""
    if field_value:
        return field_value.startswith(comparsion_value)
    return False
