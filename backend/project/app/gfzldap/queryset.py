# SPDX-FileCopyrightText: 2021
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""Queryset for filter ldap models."""

import collections.abc

from .exceptions import ModelNotFoundException


class QuerySet(collections.abc.Sequence):
    """
    Queryset for the ldap models.

    The idea is to provide a queryset, that works similar to
    the django queryset (filter, exclude, get, first).

    For the Sequence interface we need
    to implement our own __init__,
    __getitem__ and __len__
    See https://github.com/python/cpython/blob/3.10/Lib/_collections_abc.py#L1015
    """

    def __init__(self, elements, filter_options):
        """
        Init the object.

        We get two different kinds of data.
        The first one are the list of elements that
        we have so far.

        Ldap searches are done in one run and the filtering
        is done on our client side.

        The filter_options is a dict with various filter functions.
        It tries to mimik django querysets as well.

        So if you have a model with a foo field, then you can
        filter with Model.objects.fitler(foo=value).
        """
        self.elements = elements
        self.filter_options = filter_options

    def __getitem__(self, index):
        """Return the element at a given index."""
        return self.elements[index]

    def __len__(self):
        """Return the number of elements."""
        return len(self.elements)

    # Overwrite the count method from the abstract base class
    # which can be used to count the number of occurances
    # for a given element.
    # We want to have a count() method for the overall count
    # instead.
    def count(self):
        """Return the number of elements."""
        return len(self.elements)

    # We also want a different __reversed__ as still want
    # to have the result in our wrapper here.
    def __reversed__(self):
        """Return a reverse the queryset (new one)."""
        reversed_elements = reversed(self.elements)
        return QuerySet(reversed_elements, self.filter_options)

    # Now we have our own methods to work with it
    # a bit similar to a django queryset.
    def first(self):
        """Return the first entry or None."""
        if self.elements:
            return self.elements[0]
        return None

    def filter(self, **kwargs):
        """Run a filter to get a new queyset with a subset of elements."""

        def accept(comparision_value):
            return bool(comparision_value)

        return self._filter(_accept=accept, **kwargs)

    def get(self, **kwargs):
        """Return exaclty one element or raise ModelNotFoundException."""
        filtered = self.filter(**kwargs)
        if not filtered.elements:
            raise ModelNotFoundException()
        return filtered.elements[0]

    def exclude(self, **kwargs):
        """Return a filtered queyset (inverted filter functions)."""

        def reject(comparision_value):
            return not bool(comparision_value)

        return self._filter(_accept=reject, **kwargs)

    def _filter(self, _accept, **kwargs):
        """Run the main filter logic for filter and exclude."""
        all_filter_functions = {}
        for key, value in kwargs.items():
            if key in self.filter_options:
                filter_function = self.filter_options[key]
                all_filter_functions[key] = lambda x: _accept(filter_function(x, value))

        filtered_results = []
        for element in self.elements:
            if all([f(element) for f in all_filter_functions.values()]):
                filtered_results.append(element)
        return QuerySet(filtered_results, self.filter_options)

    def __repr__(self):
        """Return a string representation of the queryset."""
        inner = [repr(x) for x in self.elements[:3]]
        more = len(self.elements) > 3
        if more:
            inner.append("...")
        inner_text = ", ".join(inner)
        return f"QuerySet([{inner_text}])"
