# SPDX-FileCopyrightText: 2021
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""Base model for all the ldap models."""
import abc
import inspect

from ..utils.classes import classproperty
from ..utils.functions import FieldFunctionWrapper
from .fields import BaseField
from .objectmanager import ObjectManager


class LdapBaseModel(abc.ABC):
    """
    This is the abstract base class for the ldap models.

    It is meant to give us similar behaviour as the django base models
    with default values, with base implementations for init, eq and repr,
    as well as access to the object manager.
    """

    def __init__(self, **kwargs):
        """
        Init the object.

        Set the values based on the fields of the real model implementations.
        Also handles default values.
        """
        fields = self._get_fields()
        for field in fields:
            field_name, field_instance = field
            value = kwargs.get(field_name, field_instance.get_default())
            setattr(self, field_name, value)

    def __eq__(self, other):
        """
        Return true if equals.

        Base implementation for the eq method based on the fields.
        """
        if not isinstance(other, type(self)):
            return False
        fields = self._get_fields()
        for field in fields:
            field_name = field[0]
            if getattr(self, field_name) != getattr(other, field_name):
                return False
        return True

    def __repr__(self):
        """
        Return the string representation.

        Base implementation for the repr method based on the fields.
        """
        pk_field = self._get_pk_python_field()
        pk_value = getattr(self, pk_field)
        cls_name = self.__class__.__name__
        return f"{cls_name}({pk_field}={repr(pk_value)})"

    @classmethod
    def _from_ldap_dict(cls, ldap_dict):
        """Full the model with the content of the ldap dict."""
        fields = cls._get_fields()
        values = {}
        for field in fields:
            field_name, field_instance = field
            name_to_lookup = field_name
            if field_instance.source:
                name_to_lookup = field_instance.source
            values[field_name] = field_instance.extract_from_ldap_dict(
                ldap_dict, name_to_lookup
            )
        return cls(**values)

    @classmethod
    def _get_fields(cls):
        """Return all fields that the implementation uses."""
        return [x for x in inspect.getmembers(cls) if isinstance(x[1], BaseField)]

    @classmethod
    def _get_filters(cls):
        """Return all the filters that we can support for the .objects.filter() method."""
        filters = {}
        for field in cls._get_fields():
            field_name, field_instance = field
            for (
                filter_postfix,
                field_filter_function,
            ) in field_instance.get_filters().items():
                # filter_name = "cn" + "__null"
                filter_name = field_name + filter_postfix
                # In the way we currently use the filters, is to define
                # them on the field level (the fields now how they can
                # work with their values).
                # However, we need to work with the filters on the whole
                # model level. And doing so, we need to construct some
                # new functions.
                # With defs & lambdas however, we run into a problem
                # with the lexical scoping here (the field_name was
                # then the name of the last field in the model in the moment
                # we want to run our functions).
                filters[filter_name] = FieldFunctionWrapper(
                    field_name, field_filter_function
                )

        return filters

    @classmethod
    def _get_pk_ldap_field(cls):
        """
        Return the name of the primary key field.

        Can be different from the one that we use in python
        due to the usage of the source attribute in a field.

        The ldap pk is used for filter in the ldap query itself.
        """
        fields = cls._get_fields()
        for field in fields:
            field_name, field_instance = field
            if field_instance.primary_key:
                name_to_lookup = field_name
                if field_instance.source:
                    name_to_lookup = field_instance.source
                return name_to_lookup

    @classmethod
    def _get_pk_python_field(cls):
        """
        Return the name of the primary key field.

        As the python version in the model can be different
        from the one for the ldap (source attribute of the field),
        this here is the python version.
        It is meant to allow queries like .objects.get() to directly
        call the raw_query for one single ldap entry.
        """
        fields = cls._get_fields()
        for field in fields:
            field_name, field_instance = field
            if field_instance.primary_key:
                return field_name

    @classproperty
    def objects(cls):
        """
        Return the object manager.

        Use of the factory method to share the same object manager
        between different calls of .objects.

        Also is solved as a classproperty.
        """
        return ObjectManager.get_instance(cls)
