# SPDX-FileCopyrightText: 2021
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""
Various field classes to work with in the ldap model.

The idea is to provide field classes similar to the
django database fields.
"""

import datetime
import typing

import pydantic

from ..utils.dates import parse_YYYYMMDD_to_date
from . import filters


class BaseField(pydantic.BaseModel):
    """
    Base class for all fields.

    Note: models will not recognise fields that
    are not inherited by this class.

    Base functionality is to provide an optional source attribute
    that is then used to read from the ldap dict.
    If no source is given, it will use the name of the variable
    in the model class.

    Primary_key is used to filter for exactly one element in the ldap.
    """

    source: typing.Optional[str] = None
    primary_key = False


class StringField(BaseField):
    """String field."""

    default: typing.Optional[str] = None

    def extract_from_ldap_dict(self, ldap_dict, key):
        """Extract the data from the ldap dict."""
        result = self.get_default()
        if key in ldap_dict.keys():
            list_with_byte_strings = ldap_dict[key]
            if list_with_byte_strings:
                first_byte_string = list_with_byte_strings[0]
                result = first_byte_string.decode("utf-8")
        return result

    def get_default(self):
        """
        Return the default value.

        The default value is used to provide a default value
        for the fields in the __init__ method of the models.
        """
        return self.default

    def get_filters(self):
        """
        Return the set of supported filters.

        The filter will later be composed with the name of
        the field.
        So if we have a model:
        class DummyModel(LdapBaseModel):
            field = StringField()

        we can provide filter to run
        DummyModel.objects.filter(field__startswith='value')

        The filter that we provide here compelete work only
        on the the instanciated string only.

        So all of the filters have the form:
        def shorter(field_value, comparsion_value):
            return len(field_value) < len(comparsion_value)
        """
        return {
            "": filters.equals,
            "__in": filters.contains,
            "__null": filters.is_null_or_empty,
            "__startswith": filters.startswith,
        }


class BooleanField(BaseField):
    """Boolean Field."""

    default: bool

    def extract_from_ldap_dict(self, ldap_dict, key):
        """Extract the data from the ldap dict."""
        result = self.get_default()
        if key in ldap_dict.keys():
            list_with_byte_strings = ldap_dict[key]
            if list_with_byte_strings:
                first_byte_string = list_with_byte_strings[0]
                # The ldap values here are TRUE and FALSE as boolean strings.
                result = first_byte_string == b"TRUE"
        return result

    def get_default(self):
        """Return the default value for the field."""
        return self.default

    def get_filters(self):
        """Return the supported filters."""
        return {
            "": filters.equals,
        }


class ArrayField(BaseField):
    """Array field for lists."""

    base_field: BaseField

    def extract_from_ldap_dict(self, ldap_dict, key):
        """Extract the list from the ldap dict."""
        result = self.get_default()
        if key in ldap_dict.keys():
            list_with_byte_strings = ldap_dict[key]
            for byte_string in list_with_byte_strings:
                result.append(
                    self.base_field.extract_from_ldap_dict(
                        ldap_dict={
                            key: [byte_string],
                        },
                        key=key,
                    )
                )
        return result

    def get_default(self):
        """Start always with a fresh empty list."""
        return []

    def get_filters(self):
        """Return the supported filters."""
        return {
            "": filters.equals,
            "__contains": filters.one_of_the_values_equals,
            "__in": filters.list_contains_one,
            "__null": filters.is_null_or_empty,
        }


class DateField(BaseField):
    """Date field for YYYYMMDD formats."""

    default: typing.Optional[datetime.date] = None

    def extract_from_ldap_dict(self, ldap_dict, key):
        """Extract the date from the ldap dict."""
        result = self.get_default()
        if key in ldap_dict.keys():
            list_with_byte_strings = ldap_dict[key]
            if list_with_byte_strings:
                first_byte_string = list_with_byte_strings[0]
                as_string = first_byte_string.decode("utf-8")
                result = parse_YYYYMMDD_to_date(as_string)
                result = first_byte_string == b"TRUE"
        return result

    def get_default(self):
        """Return the default."""
        return self.default

    def get_filters(self):
        """Return the supported filters."""
        return {
            # Note: This equality filter will work with date objects,
            # and not with YYYYMMDD strings.
            "": filters.equals,
        }
