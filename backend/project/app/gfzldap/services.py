# SPDX-FileCopyrightText: 2021 - 2022
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""Higher level service functions to work with the ldap."""

from typing import Dict, List, Optional

from pydantic import BaseModel

from ..models import PermissionGroup, UserAccount
from ..utils.collections import kv_csv_to_dict
from .models import Group, Person


class LdapQueryResultForGroupsAndPeople(BaseModel):
    """Tuple class for groups & people."""

    groups: List[Group]
    people: List[Person]

    class Config:
        """Config for the pydantic model here."""

        arbitrary_types_allowed = True


def get_group_names() -> List[str]:
    """Return a list of group names."""
    return sorted([x.cn for x in Group.objects.exclude(cn__null=True)])


def get_raw_group_or_none(groupname: str) -> Optional[Dict]:
    """Return the raw dict of ldap data for a group - or None (not found)."""
    return Group.objects.raw_query_one_or_none(groupname)


def get_person_names() -> List[str]:
    """Return a list of person names."""
    return sorted(
        [
            x.uid
            # We only want those with a uid.
            for x in Person.objects.exclude(uid__null=True)
        ]
    )


def get_raw_person_or_none(username: str) -> Optional[Dict]:
    """Return the raw dict of ldap data for a person - or None (not found)."""
    return Person.objects.raw_query_one_or_none(username)


def query_ldap_for_groups_and_people() -> LdapQueryResultForGroupsAndPeople:
    """Query the ldap for groups and people."""
    all_ldap_groups = [x for x in Group.objects.all() if x.cn]
    all_ldap_people = [x for x in Person.objects.all() if x.mail]

    return LdapQueryResultForGroupsAndPeople(
        groups=all_ldap_groups,
        people=all_ldap_people,
    )


def sync():
    """Run the sync."""
    ldap_query_result = query_ldap_for_groups_and_people()
    integrate_and_update_groups_and_people(ldap_query_result)


def integrate_and_update_groups_and_people(
    ldap_query_result: LdapQueryResultForGroupsAndPeople,
):
    """Integrate and update the data in our local db."""
    all_ldap_groups = ldap_query_result.groups
    all_ldap_people = ldap_query_result.people

    for ldap_person in all_ldap_people:
        # We want to have accounts for all the people.
        user_account, created = UserAccount.objects.get_or_create(
            username=ldap_person.mail,
            # For the moment we use the very same username & displayname.
            # Later we will change it.
            displayname=ldap_person.mail,
        )
        # And only don't expose them if they are disabled.
        # Note: Currently this doesn't mean that the person will
        # be exposed automatically. There is still the need to
        # be part of an exposed group.
        user_account.expose = not ldap_person.gfz_account_disabled
        user_account.save()

    # In the group data we just have the data of the gfz username
    # (no @gfz-potsdam.de).
    # So we want to create a lookup table for all of them.
    person_lookup_table = {}
    for user_account in UserAccount.objects.all():
        # Users that are outside of the GFZ, don't need to be
        # handled by this mechanism.
        # They are then part of the mgrp_rfc822_mail_member field
        # with their full mail address.
        gfz_username = user_account.username.replace("@gfz-potsdam.de", "")
        person_lookup_table[gfz_username] = user_account

    for ldap_group in all_ldap_groups:
        members = []
        admins = []
        # members, admins & owners are always strings like this
        # uid=username,ou=People,dc=gfz-potsdam,dc=de
        for member in ldap_group.unique_members:
            member_dict = kv_csv_to_dict(member)
            uid = member_dict["uid"]
            if uid in person_lookup_table.keys():
                members.append(person_lookup_table[uid])
        for admin in ldap_group.gfz_group_admins:
            admin_dict = kv_csv_to_dict(admin)
            uid = admin_dict["uid"]
            if uid in person_lookup_table.keys():
                admins.append(person_lookup_table[uid])
        # The owner should be handled as admin as well.
        if ldap_group.owner:
            admin_dict = kv_csv_to_dict(ldap_group.owner)
            uid = admin_dict["uid"]
            if uid in person_lookup_table.keys():
                admins.append(person_lookup_table[uid])

        # And for all those external users, we want to make sure
        # that there is a user account.
        for external_member in ldap_group.mgrp_rfc822_mail_member:
            user_account, created = UserAccount.objects.get_or_create(
                username=external_member,
                # For the moment we use the very same username & displayname.
                # Later we will change it.
                displayname=external_member,
            )
            user_account.expose = True
            user_account.save()
            # Those can't be admins of the ldap group, so we handle
            # them as normal members.
            members.append(user_account)

        permission_group, created = PermissionGroup.objects.get_or_create(
            name=ldap_group.cn
        )
        if not created:
            # For new ones this is automatically False.
            # But for the old ones we want to stay with the existing
            # value, as long as it is possible (group in ldap still
            # readable and neither deactived nor hidden)
            permission_group.expose = (
                permission_group.expose and ldap_group.is_visible()
            )

        permission_group.description = ldap_group.get_description()
        permission_group.members.set(members)
        permission_group.admins.set(admins)
        permission_group.save()
