# SPDX-FileCopyrightText: 2021
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""Objectmanager for the ldap models."""
import ldap
from django.conf import settings

from .exceptions import ModelNotFoundException
from .queryset import QuerySet


class ObjectManager:
    """
    Object manager for ldap models.

    The idea is to provide a similar interface
    as the django database models with model.objects.all().
    """

    LOOKUP_OBJECT_MANAGER_BY_CLASS_NAME = {}

    def __init__(self, managed_class):
        """Init the object manager."""
        self.managed_class = managed_class

    @property
    def connection(self):
        """
        Return the connection.

        We use it as a property, as it is easy to access.
        """
        return self.connect()

    def connect(self):
        """
        Connect to the ldap.

        Additionally to the property we have here a connect method.
        This is as the connect method can be mocked easily, while
        a property is different in the unittest.mock.patch.
        """
        url = settings.LDAP_URL
        return ldap.initialize(url)

    def raw_query_all(self):
        """Return a generator with all the ldap dicts with data."""
        ldap_answer_list = self.connection.search_s(
            self.managed_class.Meta.query, ldap.SCOPE_ONELEVEL
        )
        for ldap_answer_entry in ldap_answer_list:
            query_information, model_information = ldap_answer_entry
            yield model_information

    def raw_query_one_or_none(self, pk):
        """Return the ldap dict that we queried for - or None."""
        ldap_answer_list = self.connection.search_s(
            self.managed_class.Meta.query,
            ldap.SCOPE_ONELEVEL,
            f"({self.managed_class._get_pk_ldap_field()}={pk})",
        )
        for ldap_answer_entry in ldap_answer_list:
            query_information, model_information = ldap_answer_entry
            return model_information
        return None

    def all(self):
        """Query all the data from ldap and transform to models."""
        result = []
        for raw_entry in self.raw_query_all():
            result.append(self.managed_class._from_ldap_dict(raw_entry))
        filter_options = self.managed_class._get_filters()
        return QuerySet(result, filter_options)

    def filter(self, **kwargs):
        """Query a subset of all of the ldap data and transform to models."""
        return self.all().filter(**kwargs)

    def get(self, pk):
        """Get exactly the entry we queried for (model) or raise ModelNotFoundException."""
        model = self.get_or_none(pk)
        if not model:
            raise ModelNotFoundException(pk)
        return model

    def get_or_none(self, pk):
        """Get exactly the entry we queried as model or None (not found)."""
        raw_entry = self.raw_query_one_or_none(pk)
        if not raw_entry:
            return None
        return self.managed_class._from_ldap_dict(raw_entry)

    def exclude(self, **kwargs):
        """Query a subset by excluding and transform to models."""
        return self.all().exclude(**kwargs)

    def count(self):
        """Return the overall count of entries."""
        return self.all().count()

    @classmethod
    def get_instance(cls, managed_class):
        """
        Run the factory method to share the manager over multiple model imports.

        This is a helper to make testing easier.
        """
        class_name = managed_class.__name__
        if class_name in cls.LOOKUP_OBJECT_MANAGER_BY_CLASS_NAME:
            return cls.LOOKUP_OBJECT_MANAGER_BY_CLASS_NAME[class_name]
        instance = cls(managed_class)
        cls.LOOKUP_OBJECT_MANAGER_BY_CLASS_NAME[class_name] = instance
        return instance
