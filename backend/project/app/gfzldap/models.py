# SPDX-FileCopyrightText: 2021
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""Models to access the ldap."""

from .base import LdapBaseModel
from .fields import ArrayField, BooleanField, StringField


class Person(LdapBaseModel):
    """
    Person model.

    This currently reads 3 fields from the ldap.
    1) the uid - which is the gfz username.
    2) the mail - which is the username + @gfz-potsdam.de
       it is not the full mail with first name, last name @ gfz
    3) the info if this account is deactivated
       we don't want to expose deactivated accounts.
    """

    uid = StringField(primary_key=True)
    mail = StringField()
    gfz_account_disabled = BooleanField(source="gfzAccountDisabled", default=False)

    class Meta:
        """Meta class for the Person model."""

        query = "ou=People,dc=gfz-potsdam,dc=de"


class Group(LdapBaseModel):
    """
    Group model.

    Here we read a lot of data from the gfz ldap.
    basically we use the variable name to read - if we can't,
    we define the source attribute in the field.

    The cn is the overall group name, that can be seen as a primary key.

    Owner is one string of person data, unique_members and gfz_group_admins contains
    lists of them.
    It looks like this:
        uid=username,ou=People,dc=gfz-potsdam,dc=de

    The mgrp_rfc822_mail_member contains lists of full email addresses
    (for external users).

    The rest are nameing information (there can be various variants on which of
    them are set) and visiblity information (readable, hide, disabled).
    """

    cn = StringField(primary_key=True)
    owner = StringField()
    gfz_group_disabled = BooleanField(source="gfzGroupDisabled", default=False)
    gfz_group_readable = BooleanField(source="gfzGroupReadable", default=True)
    gfz_group_hide = BooleanField(source="gfzGroupHide", default=True)
    gfz_group_de = StringField(source="gfzGroupDE")
    gfz_group_en = StringField(source="gfzGroupEN")
    gfz_group_display_name_de = StringField(source="gfzGroupDisplayNameDE")
    gfz_group_display_name_en = StringField(source="gfzGroupDisplayNameEN")
    unique_members = ArrayField(base_field=StringField(), source="uniqueMember")
    gfz_group_admins = ArrayField(base_field=StringField(), source="gfzGroupAdmin")
    mgrp_rfc822_mail_member = ArrayField(
        base_field=StringField(), source="mgrpRFC822MailMember"
    )

    class Meta:
        """Meta class for the group model."""

        query = "ou=groups,dc=gfz-potsdam,dc=de"

    def get_description(self):
        """
        Return a description.

        Prefer english over german, and longer description over shorter ones.
        """
        return (
            self.gfz_group_display_name_en
            or self.gfz_group_display_name_de
            or self.gfz_group_en
            or self.gfz_group_de
            or self.cn
        )

    def is_visible(self):
        """Return if we can allow to show the group."""
        return self.gfz_group_readable and not (
            self.gfz_group_hide or self.gfz_group_disabled
        )
