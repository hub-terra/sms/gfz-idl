# SPDX-FileCopyrightText: 2021
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""Exceptions for the ldap usage."""


class LdapBaseException(Exception):
    """Base class for all exceptions for the ldap uasge."""

    pass


class ModelNotFoundException(LdapBaseException):
    """Model was not found (but requested)."""

    pass
