# SPDX-FileCopyrightText: 2021 - 2022
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""Admin interface for the app."""
from django.contrib import admin

from .models import PermissionGroup

# Register your models here.


class PermissionGroupAdmin(admin.ModelAdmin):
    """Admininterface for the PermissionGroups."""

    search_fields = ("name",)
    list_display = ("name", "src", "expose")
    list_filter = ("expose",)
    # Just the expose as editable field.
    readonly_fields = ("name", "src", "description", "admins", "members")

    def has_delete_permission(self, request, obj=None):
        """
        Check if we should allow to delete in the admin view.

        Currently we don't want that, as all the groups should
        be managed with the gfz myprofile.
        """
        return False

    def has_add_permission(self, request):
        """
        Check if we should allow to create new groups.

        Similar to the delete, we don't want to have changes
        here from the admin interface. All the groups should
        be managed by the myprofile and synced here via ldap.
        So no manual groups.
        """
        return False


admin.site.register(PermissionGroup, PermissionGroupAdmin)
