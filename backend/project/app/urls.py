# SPDX-FileCopyrightText: 2021 - 2022
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""Urls for the IDL."""

from django.urls import path

from . import views
from .hifis.views import SyncGroupsFromOpenIdConnectView

urlpatterns = [
    path(
        "api/idl/permission-groups/",
        views.IdlApiPermissionGroupViewSet.as_view({"get": "list"}),
        name="permission-group-list",
    ),
    path(
        "api/idl/permission-groups/<str:pk>/",
        views.IdlApiPermissionGroupViewSet.as_view({"get": "retrieve"}),
        name="permission-group-detail",
    ),
    path(
        "api/idl/user-accounts/",
        views.IdlApiUserAccountViewSet.as_view({"get": "list"}),
        name="user-account-list",
    ),
    path(
        "api/idl/user-accounts/<str:pk>/",
        views.IdlApiUserAccountViewSet.as_view({"get": "retrieve"}),
        name="user-account-detail",
    ),
    path(
        "api/hifis/sync-groups/",
        SyncGroupsFromOpenIdConnectView.as_view(),
        name="sync-groups",
    ),
    path("", views.IndexView.as_view(), name="index"),
    path("swagger-ui/", views.SwaggerUIView.as_view(), name="swagger-ui"),
    path("openapi.json/", views.OpenApiView.as_view(), name="openapi"),
]
