# SPDX-FileCopyrightText: 2022
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""
Permission classes for the idl.

Here we handle the logic if a request is allowed
to request / change a certain resource.
"""

from django.conf import settings
from rest_framework.exceptions import PermissionDenied


class HasIdlToken:
    """
    Permission class to check if the user uses an idl token.

    The token should be send in the Authorization header
    with a value like:
    "Bearer {token}"
    """

    def raise_exception_if_not_allowed(self, request):
        """Raise an exception if the request is not allowed."""
        headers = request.headers
        if not self._has_idl_token(headers):
            raise PermissionDenied("No valid token to use the idl")

    def _has_idl_token(self, headers):
        """Return true if the idl token is in the request headers."""
        key = "Authorization"
        if key not in headers.keys():
            return False
        value = headers.get(key)
        if not value:
            return False
        if not value.startswith("Bearer"):
            return False
        token_value = value.replace("Bearer", "").strip()
        if not self._is_idl_token(token_value):
            return False
        return True

    def _is_idl_token(self, token_value):
        """
        Check if the token is the idl token.

        For the moment we just support one single token,
        that is defined in the env variables and can be
        set on the startup of the application.
        """
        return token_value == settings.IDL_TOKEN
