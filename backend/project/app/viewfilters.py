# SPDX-FileCopyrightText: 2021 - 2022
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""Filters to work with in the views."""

from functools import reduce

from django.db.models import Q


class FilterFieldEquals:
    """
    Filter factory for list views.

    This class here constructs a callable instance that
    can be used in the allowed_list_filters.
    It basically checks for equality, by extracting
    the query parameter value and filters them based
    on equality.
    """

    def __init__(self, query_parameter_name, model_field):
        """
        Init the object.

        Query parameter name can differ from the model field,
        so there are two differnt arguments for the init.
        """
        self.query_parameter_name = query_parameter_name
        self.model_field = model_field

    def __call__(self, queryset, query_params):
        """Apply the filter for the queryset if the parameter is set."""
        query_parameter_value = query_params.get(self.query_parameter_name)
        if query_parameter_value:
            filter_dict = {self.model_field: query_parameter_value}
            queryset = queryset.filter(**filter_dict)
        return queryset


class FilterFieldIn:
    """
    Filter factory to run xyz_in queries.

    This filter is similar to the FilterFieldEquals, but it
    allows us to check for multiple parameter values.
    So, for example for the `id_is[]` parameter:

    GET /endpoint?id_is[]=3&id_is[]=4

    Will return us all the entries that either have the id 3 or the
    id 4. It will use or-semantic here.
    """

    def __init__(self, query_parameter_name, model_field):
        """Init the filter info."""
        self.query_parameter_name = query_parameter_name
        self.model_field = model_field

    def __call__(self, queryset, query_params):
        """Apply the filter for the given values."""
        query_parameter_list = query_params.getlist(self.query_parameter_name)
        if query_parameter_list:
            filter_dict = {f"{self.model_field}__in": query_parameter_list}
            queryset = queryset.filter(**filter_dict)
        return queryset


class FilterSearchMultipleStringsMultipleFields:
    """
    Filter factory for list views for text searchs.

    This class here constructs a callable interface
    that can be used in the allowed_list_filters.
    It splits the the value by whitespace & tries
    to find all of them one one of the fields.
    The search itself is case insensitive.
    """

    def __init__(self, query_parameter_name, model_fields):
        """
        Init the filter.

        Takes the query_parameter_name (in the request) and
        the list of fields (each one as strings) to search in.
        """
        self.query_parameter_name = query_parameter_name
        self.model_fields = model_fields

    def __call__(self, queryset, query_params):
        """Apply the filter."""
        query_parameter_value = query_params.get(self.query_parameter_name)
        if query_parameter_value:
            # We want to have every word included in one of the fields.
            for word in query_parameter_value.split():
                queryset = queryset.filter(
                    self._create_condition_find_word_in_one_of_the_fields(word)
                )
        return queryset

    def _create_condition_find_word_in_one_of_the_fields(self, word):
        or_conditions = self._create_or_conditions_to_find(word)
        return reduce(lambda a, b: a | b, or_conditions)

    def _create_or_conditions_to_find(self, word):
        return [
            self._create_condition_to_find_in_field(word, field)
            for field in self.model_fields
        ]

    @staticmethod
    def _create_condition_to_find_in_field(word, field):
        as_dict = {field + "__icontains": word}
        return Q(**as_dict)
