# SPDX-FileCopyrightText: 2021 - 2022
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""Views for the IDL."""
from django.db.models import Q
from django.shortcuts import get_object_or_404
from django.views.generic import TemplateView
from rest_framework import viewsets
from rest_framework.response import Response

from .models import PermissionGroup, UserAccount
from .permissions import HasIdlToken
from .schemas import PermissionGroupSchema, UserAccountSchema
from .viewfilters import (
    FilterFieldEquals,
    FilterFieldIn,
    FilterSearchMultipleStringsMultipleFields,
)


class IndexView(TemplateView):
    """View for the index page."""

    template_name = "app/index.html"


class SwaggerUIView(TemplateView):
    """View for the swagger page."""

    template_name = "app/swagger-ui.html"


class OpenApiView(TemplateView):
    """View for the openapi json payload."""

    template_name = "app/openapi.json"


class PaginationMixin:
    """Mixin to handle pagination."""

    ITEMS_PER_PAGE = 100

    def paginate(self, queryset, request):
        """Paginate the queryset based in query parameters."""
        skip_pagination = request.query_params.get("pagination") in ["false"]
        if skip_pagination:
            return queryset
        page = int(request.query_params.get("page", 1))
        items_per_page = int(
            request.query_params.get("itemsPerPage", self.ITEMS_PER_PAGE)
        )
        start = max(0, (page - 1) * items_per_page)
        return queryset[start : (start + items_per_page)]


class BaseIdlApiViewSet(viewsets.ViewSet, PaginationMixin):
    """
    A basic viewset for all of our models.

    It relies that the implementations provide some attriubutes:
    - queryset - for example Model.objects.all()
    - schema - an object that has a serialize_instance_to_dict method.
    - allowed_list_filters - a list with functions to filter the queryset

    Filters by query parameters can be applied by overwriting the
    apply_filters_by_query_params method.
    """

    def list(self, request):
        """Return the list for a get /<model>/."""
        for permission_check in self.general_permissions:
            permission_check.raise_exception_if_not_allowed(request)
        queryset = self.queryset
        filtered_queryset = self.apply_filters_by_query_params(
            queryset, request.query_params
        )
        paginated_queryset = self.paginate(filtered_queryset, request)
        result = []
        for instance in paginated_queryset:
            result.append(self.schema.serialize_instance_to_dict(instance))
        return Response(result)

    def retrieve(self, request, pk=None):
        """Return the details for a get /<model>/<pk>/."""
        for permission_check in self.general_permissions:
            permission_check.raise_exception_if_not_allowed(request)
        queryset = self.queryset
        instance = get_object_or_404(queryset, pk=pk)
        return Response(self.schema.serialize_instance_to_dict(instance))

    def apply_filters_by_query_params(self, queryset, query_params):
        """Apply additional filters based on the query parameters."""
        for filter in self.allowed_list_filters:
            queryset = filter(queryset, query_params)
        return queryset


class IdlApiPermissionGroupViewSet(BaseIdlApiViewSet):
    """Viewset for the Permission groups."""

    schema = PermissionGroupSchema(only_exposed_users=True)
    queryset = PermissionGroup.objects.filter(expose=True)
    general_permissions = [HasIdlToken()]
    allowed_list_filters = [
        FilterFieldEquals(query_parameter_name="id_is", model_field="id"),
        FilterSearchMultipleStringsMultipleFields(
            query_parameter_name="search", model_fields=["name", "description"]
        ),
        FilterFieldIn(query_parameter_name="id_is[]", model_field="id"),
    ]


class IdlApiUserAccountViewSet(BaseIdlApiViewSet):
    """Viewset for the User accounts."""

    schema = UserAccountSchema(only_exposed_groups=True)
    queryset = UserAccount.objects.filter(expose=True).filter(
        Q(administrated_group__expose=True) | Q(membered_group__expose=True)
    )
    general_permissions = [HasIdlToken()]
    allowed_list_filters = [
        FilterFieldEquals(query_parameter_name="userName_is", model_field="username"),
        FilterFieldEquals(query_parameter_name="id_is", model_field="id"),
        FilterSearchMultipleStringsMultipleFields(
            query_parameter_name="search", model_fields=["username", "displayname"]
        ),
        FilterFieldIn(query_parameter_name="id_is[]", model_field="id"),
    ]
