# SPDX-FileCopyrightText: 2021 - 2022
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""Database models for the app."""
from django.db import models

# Create your models here.


class UserAccount(models.Model):
    """
    User account.

    We are very sparse with the data that we need for a user account.
    Just the bare minimum.
    Basically we want to store just the username, which follows
    the same structure as the sub that we get from the IDP in the sms.
    This is <gfz-username>@gfz-potsdam.de.

    The expose is to hide users. In general users are exposed by
    default. But it is possible to deactivate a user in the ldap.
    When the user is deactivated we want to stay with its entry,
    as deleting it, and reinserting would result in a different id.
    By staying with the id, we can be sure that a reactivated user
    will not lose the bindings to the groups.
    """

    username = models.CharField(max_length=256, blank=False, null=False)
    displayname = models.CharField(max_length=256)
    expose = models.BooleanField(default=True)

    class Meta:
        """Meta class for the UserAccount."""

        ordering = ["username"]
        indexes = [models.Index(fields=["username"])]
        constraints = [
            models.UniqueConstraint(
                fields=["username"], name="user_account_username_unique_constraint"
            ),
        ]

    def __str__(self):
        """
        Return a string representation of the user account.

        As this is the name that will be shown in the django admin
        interface, we just use the username.
        """
        return self.username


class PermissionGroup(models.Model):
    """
    Permission group.

    Combines users with permissions (member, admin) in one group.
    As for user accounts, we also have here an expose flag.
    This flag will control if the group will be exposed by
    the API.
    As we sync with LDAP, we get a lot of groups. And for sure
    most of them are not relevant for the DataHub software projects.
    With this flag, we can control this.

    Similar to the users, it can also that groups will be deactivated
    in the ldap. And as for the users, we want to stay with those
    groups in case they will be re-activated. (And in fact it is
    more importent here, if a client of the IDL will save just
    the ids of the groups.)

    Name here is just the name of the group in the gfz ldap.
    """

    name = models.CharField(max_length=256, blank=False, null=False)
    description = models.TextField()
    src = models.CharField(max_length=256, blank=True, null=True)
    admins = models.ManyToManyField(
        "UserAccount",
        related_name="administrated_groups",
        related_query_name="administrated_group",
        blank=True,
    )
    members = models.ManyToManyField(
        "UserAccount",
        related_name="membered_groups",
        related_query_name="membered_group",
        blank=True,
    )
    expose = models.BooleanField(default=False)

    class Meta:
        """Meta class for the permission groups."""

        ordering = ["name"]
        constraints = [
            models.UniqueConstraint(
                fields=["name", "src"],
                name="permission_group_name_src_unique_constraint",
            ),
        ]

    def __str__(self):
        """
        Return a string representation of the group.

        As this is the name that will be shown in the django admin
        interface, we just use the name.
        """
        return self.name
