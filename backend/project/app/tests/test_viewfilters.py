# SPDX-FileCopyrightText: 2021 - 2022
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""Test the filters for the views."""

from django.test import TestCase

from ..models import PermissionGroup, UserAccount
from ..viewfilters import (
    FilterFieldEquals,
    FilterFieldIn,
    FilterSearchMultipleStringsMultipleFields,
)


class MockQueryParameterDict:
    """Test implementation to provide QueryParameterDict methods."""

    def __init__(self, data):
        """Init this mock with data."""
        self.data = data

    def getlist(self, key):
        """Return the data of the dict or an empty list."""
        return self.data.get(key, [])


class TestMockQueryParameterDict(TestCase):
    """Tests for the MockQueryParameterDict class."""

    def test_getlist_empty(self):
        """Test the getlist method without any data."""
        data = {}
        mocked = MockQueryParameterDict(data)
        result = mocked.getlist("entry")
        self.assertEqual(result, [])

    def test_getlist_with_entries(self):
        """Test the getlist method with some data."""
        data = {"entry": [1, 2, 3]}
        mocked = MockQueryParameterDict(data)
        result = mocked.getlist("entry")
        self.assertEqual(result, [1, 2, 3])


class TestFilterFieldEquals(TestCase):
    """Tests for the FilterFieldEquals class."""

    def test_with_user_account_usernames(self):
        """Test the filtering for user accounts by username."""
        UserAccount.objects.create(username="u1", expose=True)
        UserAccount.objects.create(username="u2", expose=True)

        filter = FilterFieldEquals(
            query_parameter_name="username_is", model_field="username"
        )

        full_result = filter(UserAccount.objects.all(), {})
        self.assertEqual(full_result.count(), 2)

        filtered_result = filter(UserAccount.objects.all(), {"username_is": "u1"})
        self.assertEqual(filtered_result.count(), 1)
        self.assertEqual(filtered_result[0].username, "u1")

        empty_result = filter(UserAccount.objects.all(), {"username_is": "uuuu"})
        self.assertEqual(empty_result.count(), 0)

    def test_with_permission_group_ids(self):
        """Test the filtering the permission groups by id."""
        abc_group = PermissionGroup.objects.create(
            name="ABC", description="abc desc.", expose=True
        )
        def_group = PermissionGroup.objects.create(
            name="DEF", description="def desc.", expose=True
        )
        filter = FilterFieldEquals(query_parameter_name="id_is", model_field="id")

        full_result = filter(PermissionGroup.objects.all(), {})
        self.assertEqual(full_result.count(), 2)

        filtered_result = filter(PermissionGroup.objects.all(), {"id_is": abc_group.id})
        self.assertEqual(filtered_result.count(), 1)
        self.assertEqual(filtered_result[0].id, abc_group.id)

        empty_result = filter(PermissionGroup.objects.all(), {"id_is": -123})
        self.assertEqual(empty_result.count(), 0)


class TestFilterSearchMultipleStringsMultipleFields(TestCase):
    """Tests for the FilterSearchMultipleStringsMultipleFields class."""

    def test_with_name_and_description(self):
        """Test the search in name and description."""
        abc_group = PermissionGroup.objects.create(
            name="ABC",
            description="A longer description for the first group.",
            expose=True,
        )
        def_group = PermissionGroup.objects.create(
            name="DEF", description="Shorter description for second one.", expose=True
        )
        filter = FilterSearchMultipleStringsMultipleFields(
            query_parameter_name="search", model_fields=["name", "description"]
        )

        full_result = filter(PermissionGroup.objects.all(), {})
        self.assertEqual(full_result.count(), 2)

        filtered_result = filter(PermissionGroup.objects.all(), {"search": "ABC"})
        self.assertEqual(filtered_result.count(), 1)
        self.assertEqual(filtered_result[0].name, "ABC")

        empty_result = filter(PermissionGroup.objects.all(), {"search": "uuuuuu"})
        self.assertEqual(empty_result.count(), 0)

        filtered_result_complex = filter(
            PermissionGroup.objects.all(), {"search": "de seCond"}
        )
        self.assertEqual(filtered_result_complex.count(), 1)
        self.assertEqual(filtered_result_complex[0].name, "DEF")


class TestFilterFieldIn(TestCase):
    """Tests for the FilterFieldIn class."""

    def setUp(self):
        """Set up the tests."""
        self.u1 = UserAccount.objects.create(username="u1", expose=True)
        self.u2 = UserAccount.objects.create(username="u2", expose=True)
        self.filter = FilterFieldIn(query_parameter_name="id_is[]", model_field="id")

    def test_with_empty_id_list(self):
        """
        Test with an empty id list.

        This should have the very same semantic is if no parameter is given
        at all.
        This may seem confusing at first, but we it is the same settings
        if we don't provide an "id_is[]=" parameter - so we don't apply
        the filter overall.
        """
        full_result = self.filter(
            UserAccount.objects.all(), MockQueryParameterDict({"id_is[]": []})
        )
        self.assertEqual(full_result.count(), 2)

    def test_with_two_users(self):
        """Test the id fitler with two existing user ids."""
        filtered_result = self.filter(
            UserAccount.objects.all(),
            MockQueryParameterDict({"id_is[]": [self.u1.id, self.u2.id]}),
        )
        self.assertEqual(filtered_result.count(), 2)
        self.assertIn(filtered_result[0].id, [self.u1.id, self.u2.id])
        self.assertIn(filtered_result[1].id, [self.u1.id, self.u2.id])

    def test_with_non_existing_id(self):
        """
        Test with a non existing id.

        This should return an empty queryset, as we don't have any element
        with the given id.
        """
        empty_result = self.filter(
            UserAccount.objects.all(), MockQueryParameterDict({"id_is[]": [-9999]})
        )
        self.assertEqual(empty_result.count(), 0)

    def test_with_no_filter_parameter_at_all(self):
        """
        Test with no query parameter.

        This should return the full queryset - as we don't apply any filter.
        """
        no_filter_applied = self.filter(
            UserAccount.objects.all(), MockQueryParameterDict({})
        )
        self.assertEqual(no_filter_applied.count(), 2)
        self.assertIn(no_filter_applied[0].id, [self.u1.id, self.u2.id])
        self.assertIn(no_filter_applied[1].id, [self.u1.id, self.u2.id])
