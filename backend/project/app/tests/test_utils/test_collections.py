# SPDX-FileCopyrightText: 2021
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""Tests for the utils."""

from django.test import TestCase

from ...utils import collections


class TestByteListToStrList(TestCase):
    """Test the byte_list_to_str_list function."""

    def test_input(self):
        """Test the function with a list."""
        input_list = [b"this", b"that"]
        result = collections.byte_list_to_str_list(input_list)
        self.assertEqual(result, ["this", "that"])

    def test_empty(self):
        """Test the function with an empty list."""
        input_list = []
        result = collections.byte_list_to_str_list(input_list)
        self.assertEqual(result, [])


class TestKvCsvToDict(TestCase):
    """Tests for the kv_csv_to_dict function."""

    def test_empty(self):
        """Test with an empty string."""
        test_input = ""
        result = collections.kv_csv_to_dict(test_input)
        self.assertEqual(result, {})

    def test_multiple_values(self):
        """Test with some different values."""
        test_input = "uid=abc,key=value,test=pass"
        result = collections.kv_csv_to_dict(test_input)
        expected = {"uid": "abc", "key": "value", "test": "pass"}
        self.assertEqual(result, expected)
