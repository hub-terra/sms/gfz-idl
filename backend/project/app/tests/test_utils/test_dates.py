# SPDX-FileCopyrightText: 2021
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""Tests for the date utils."""

from django.test import TestCase

from ...utils import dates


class TestParseYYYYMMDDToDate(TestCase):
    """Tests for the parse_YYYYMMDD_to_date function."""

    def test_20211124(self):
        """Test with the date when writing the tests."""
        test_input = "20211124"
        date = dates.parse_YYYYMMDD_to_date(test_input)
        self.assertEqual(date.year, 2021)
        self.assertEqual(date.month, 11)
        self.assertEqual(date.day, 24)
