# SPDX-FileCopyrightText: 2021
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""Tests for the utils for functions."""

from dataclasses import dataclass
from django.test import TestCase
from ...utils import functions


class TestFieldFunctionWrapper(TestCase):
    """Tests for the field function wrapper class."""

    def test_with_example_class(self):
        """Test with a simple class."""

        @dataclass
        class DummyClass:
            """Dummy class for the test."""

            my_str_field: str
            my_int_field: int

        d1 = DummyClass(my_str_field="foo", my_int_field=42)

        str_field_len = functions.FieldFunctionWrapper("my_str_field", len)
        self.assertEqual(str_field_len(d1), 3)

        def double(some_number):
            """Double the given number."""
            return some_number * 2

        int_field_double = functions.FieldFunctionWrapper("my_int_field", double)
        self.assertEqual(int_field_double(d1), 84)
