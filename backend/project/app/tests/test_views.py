# SPDX-FileCopyrightText: 2021 - 2022
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""Tests for the views."""

from django.conf import settings
from django.test import TestCase
from django.urls import reverse
from rest_framework import status

from ..models import PermissionGroup, UserAccount


class TestIdlApiPermissionGroupListView(TestCase):
    """Tests for the permission group list view."""

    def setUp(self):
        """
        Startup our tests.

        We want all of our tests here to use the IDL token.
        """
        self.client.defaults["HTTP_Authorization"] = f"Bearer {settings.IDL_TOKEN}"

    def test_get_without_idl_token(self):
        """
        Test without the IDL token.

        We want to make sure that we get an 403_FORBIDDEN status code.
        """
        del self.client.defaults["HTTP_Authorization"]
        response = self.client.get(reverse("permission-group-list"))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_get_empty(self):
        """Test the get method when there is no group."""
        self.assertEqual(PermissionGroup.objects.count(), 0)

        response = self.client.get(
            reverse("permission-group-list"),
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json(), [])

    def test_get_no_exposed_group(self):
        """Test the get method for only non exposed groups."""
        self.assertEqual(PermissionGroup.objects.count(), 0)
        for i in range(10):
            PermissionGroup.objects.create(name=f"Group {i+1}", expose=False)

        response = self.client.get(reverse("permission-group-list"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json(), [])

    def test_get_two_exposed_groups(self):
        """Test the get method with 2 exposed groups."""
        self.assertEqual(PermissionGroup.objects.count(), 0)

        abc_group = PermissionGroup.objects.create(
            name="ABC", description="abc desc.", expose=True
        )
        def_group = PermissionGroup.objects.create(
            name="DEF", description="def desc.", expose=True
        )

        response = self.client.get(reverse("permission-group-list"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        expected = [
            {
                "id": str(abc_group.id),
                "name": "ABC",
                "description": "abc desc.",
                "members": [],
                "admins": [],
            },
            {
                "id": str(def_group.id),
                "name": "DEF",
                "description": "def desc.",
                "members": [],
                "admins": [],
            },
        ]
        self.assertEqual(response.json(), expected)

    def test_get_one_group_with_members_and_admins(self):
        """Test the get method with one group with member and admins."""
        self.assertEqual(UserAccount.objects.count(), 0)
        self.assertEqual(PermissionGroup.objects.count(), 0)

        aaa_user = UserAccount.objects.create(username="aaa", expose=True)
        bbb_user = UserAccount.objects.create(username="bbb", expose=True)

        group = PermissionGroup.objects.create(
            name="ABC", description="abc desc.", expose=True
        )
        group.members.set([aaa_user])
        group.admins.set([bbb_user])
        group.save()

        response = self.client.get(reverse("permission-group-list"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        expected = [
            {
                "id": str(group.id),
                "name": "ABC",
                "description": "abc desc.",
                "members": [str(aaa_user.id)],
                "admins": [str(bbb_user.id)],
            },
        ]
        self.assertEqual(response.json(), expected)

    def test_get_not_exposed_members_and_admins(self):
        """Test the get method for group without exposed member and admins."""
        self.assertEqual(UserAccount.objects.count(), 0)
        self.assertEqual(PermissionGroup.objects.count(), 0)

        aaa_user = UserAccount.objects.create(username="aaa", expose=False)
        bbb_user = UserAccount.objects.create(username="bbb", expose=False)

        group = PermissionGroup.objects.create(
            name="ABC", description="abc desc.", expose=True
        )
        group.members.set([aaa_user])
        group.admins.set([bbb_user])
        group.save()

        response = self.client.get(reverse("permission-group-list"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        expected = [
            {
                "id": str(group.id),
                "name": "ABC",
                "description": "abc desc.",
                "members": [],
                "admins": [],
            },
        ]
        self.assertEqual(response.json(), expected)

    def test_get_id_is(self):
        """Test the id_is parameter, to filter for a specific id."""
        self.assertEqual(PermissionGroup.objects.count(), 0)

        abc_group = PermissionGroup.objects.create(
            name="ABC", description="abc desc.", expose=True
        )
        def_group = PermissionGroup.objects.create(
            name="DEF", description="def desc.", expose=True
        )

        response_abc = self.client.get(
            reverse("permission-group-list"), {"id_is": abc_group.id}
        )
        self.assertEqual(response_abc.status_code, status.HTTP_200_OK)
        expected_abc = [
            {
                "id": str(abc_group.id),
                "name": "ABC",
                "description": "abc desc.",
                "members": [],
                "admins": [],
            },
        ]
        self.assertEqual(response_abc.json(), expected_abc)
        response_def = self.client.get(
            reverse("permission-group-list"), {"id_is": str(def_group.id)}
        )
        self.assertEqual(response_def.status_code, status.HTTP_200_OK)
        expected_def = [
            {
                "id": str(def_group.id),
                "name": "DEF",
                "description": "def desc.",
                "members": [],
                "admins": [],
            },
        ]
        self.assertEqual(response_def.json(), expected_def)

    def test_get_id_is_array(self):
        """Test the id_is[] parameter to filter for some specific ids."""
        self.assertEqual(PermissionGroup.objects.count(), 0)

        abc_group = PermissionGroup.objects.create(
            name="ABC", description="abc desc.", expose=True
        )
        def_group = PermissionGroup.objects.create(
            name="DEF", description="def desc.", expose=True
        )

        response_abc = self.client.get(
            reverse("permission-group-list"),
            {"id_is[]": [abc_group.id, "-9999"]},
        )
        self.assertEqual(response_abc.status_code, status.HTTP_200_OK)
        expected_abc = [
            {
                "id": str(abc_group.id),
                "name": "ABC",
                "description": "abc desc.",
                "members": [],
                "admins": [],
            },
        ]
        self.assertEqual(response_abc.json(), expected_abc)
        response_abc_def = self.client.get(
            reverse("permission-group-list"),
            {"id_is[]": [abc_group.id, def_group.id]},
        )
        self.assertEqual(response_abc_def.status_code, status.HTTP_200_OK)
        expected_abc_def = [
            {
                "id": str(abc_group.id),
                "name": "ABC",
                "description": "abc desc.",
                "members": [],
                "admins": [],
            },
            {
                "id": str(def_group.id),
                "name": "DEF",
                "description": "def desc.",
                "members": [],
                "admins": [],
            },
        ]
        self.assertEqual(response_abc_def.json(), expected_abc_def)

    def test_get_search(self):
        """
        Test the search parameter, to filter some texts.

        The search parameter is a bit complex.
        It should run case insensitive & search on different fieldss with
        multiple keywords split by whitespace.
        """
        self.assertEqual(PermissionGroup.objects.count(), 0)

        abc_group = PermissionGroup.objects.create(
            name="ABC",
            description="A longer description for the first group.",
            expose=True,
        )
        def_group = PermissionGroup.objects.create(
            name="DEF", description="Shorter description for second one.", expose=True
        )

        response_abc = self.client.get(
            reverse("permission-group-list"), {"search": "abc"}
        )
        self.assertEqual(response_abc.status_code, status.HTTP_200_OK)
        expected_abc = [
            {
                "id": str(abc_group.id),
                "name": "ABC",
                "description": "A longer description for the first group.",
                "members": [],
                "admins": [],
            },
        ]
        self.assertEqual(response_abc.json(), expected_abc)
        response_def = self.client.get(
            reverse("permission-group-list"), {"search": "shorter second"}
        )
        self.assertEqual(response_def.status_code, status.HTTP_200_OK)
        expected_def = [
            {
                "id": str(def_group.id),
                "name": "DEF",
                "description": "Shorter description for second one.",
                "members": [],
                "admins": [],
            },
        ]
        self.assertEqual(response_def.json(), expected_def)

    def test_pagination(self):
        """
        Test the pagination.

        The default pagination says that we take x items in one page.
        """
        default_number_of_items_per_page = 100

        for i in range(default_number_of_items_per_page + 1):
            number = str(i + 1).zfill(3)
            PermissionGroup.objects.create(
                name=f"Group {number}",
                description="",
                expose=True,
            )
        response_default_pagination = self.client.get(reverse("permission-group-list"))
        self.assertEqual(response_default_pagination.status_code, status.HTTP_200_OK)
        response_default_pagination_data = response_default_pagination.json()
        self.assertEqual(
            len(response_default_pagination_data), default_number_of_items_per_page
        )
        self.assertEqual(response_default_pagination_data[0]["name"], "Group 001")
        self.assertEqual(response_default_pagination_data[-1]["name"], "Group 100")

        response_page2 = self.client.get(reverse("permission-group-list"), {"page": 2})
        self.assertEqual(response_page2.status_code, status.HTTP_200_OK)
        response_page2_data = response_page2.json()
        self.assertEqual(len(response_page2_data), 1)
        self.assertEqual(response_page2_data[0]["name"], "Group 101")

        response_no_pagination = self.client.get(
            reverse("permission-group-list"), {"pagination": "false"}
        )
        self.assertEqual(response_no_pagination.status_code, status.HTTP_200_OK)
        response_no_pagination_data = response_no_pagination.json()
        self.assertEqual(
            len(response_no_pagination_data), default_number_of_items_per_page + 1
        )
        self.assertEqual(response_no_pagination_data[0]["name"], "Group 001")
        self.assertEqual(response_no_pagination_data[-1]["name"], "Group 101")

        response_custom_pagination = self.client.get(
            reverse("permission-group-list"), {"itemsPerPage": 10, "page": 2}
        )
        self.assertEqual(response_custom_pagination.status_code, status.HTTP_200_OK)
        response_custom_pagination_data = response_custom_pagination.json()
        self.assertEqual(len(response_custom_pagination_data), 10)
        self.assertEqual(response_custom_pagination_data[0]["name"], "Group 011")
        self.assertEqual(response_custom_pagination_data[-1]["name"], "Group 020")


class TestIdlApiPermissionGroupDetailView(TestCase):
    """Tests for the permission group detail view."""

    def setUp(self):
        """
        Startup our tests.

        We want all of our tests here to use the IDL token.
        """
        self.client.defaults["HTTP_Authorization"] = f"Bearer {settings.IDL_TOKEN}"

    def test_get_without_idl_token(self):
        """
        Test without the IDL token.

        We want to make sure that we get an 403_FORBIDDEN status code.
        And we want to make sure that the 403 is before
        we would return an 404 (as users without the permissions
        could check if there is a resource - even if they can't
        access them).
        """
        del self.client.defaults["HTTP_Authorization"]
        response = self.client.get(reverse("permission-group-detail", args=(123,)))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_get_non_existing_group(self):
        """Test the get method when there is no group."""
        self.assertEqual(PermissionGroup.objects.count(), 0)

        response = self.client.get(reverse("permission-group-detail", args=(123,)))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_get_no_exposed_group(self):
        """Test the get method for only non exposed groups."""
        self.assertEqual(PermissionGroup.objects.count(), 0)
        group = PermissionGroup.objects.create(name=f"Group", expose=False)

        response = self.client.get(reverse("permission-group-detail", args=(group.id,)))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_get_one_group_with_members_and_admins(self):
        """Test the get method with one group with member and admins associated to."""
        self.assertEqual(UserAccount.objects.count(), 0)
        self.assertEqual(PermissionGroup.objects.count(), 0)

        aaa_user = UserAccount.objects.create(username="aaa", expose=True)
        bbb_user = UserAccount.objects.create(username="bbb", expose=True)

        group = PermissionGroup.objects.create(
            name="ABC", description="abc desc.", expose=True
        )
        group.members.set([aaa_user])
        group.admins.set([bbb_user])
        group.save()

        response = self.client.get(reverse("permission-group-detail", args=(group.id,)))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        expected = {
            "id": str(group.id),
            "name": "ABC",
            "description": "abc desc.",
            "members": [str(aaa_user.id)],
            "admins": [str(bbb_user.id)],
        }
        self.assertEqual(response.json(), expected)

    def test_get_not_exposed_members_and_admins(self):
        """Test the get method with one group with member and admins associated to."""
        self.assertEqual(UserAccount.objects.count(), 0)
        self.assertEqual(PermissionGroup.objects.count(), 0)

        aaa_user = UserAccount.objects.create(username="aaa", expose=False)
        bbb_user = UserAccount.objects.create(username="bbb", expose=False)

        group = PermissionGroup.objects.create(
            name="ABC", description="abc desc.", expose=True
        )
        group.members.set([aaa_user])
        group.admins.set([bbb_user])
        group.save()

        response = self.client.get(reverse("permission-group-detail", args=(group.id,)))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        expected = {
            "id": str(group.id),
            "name": "ABC",
            "description": "abc desc.",
            "members": [],
            "admins": [],
        }
        self.assertEqual(response.json(), expected)


class TestIdlApiUserAccountListView(TestCase):
    """Tests for the user account list view."""

    def setUp(self):
        """
        Startup our tests.

        We want all of our tests here to use the IDL token.
        """
        self.client.defaults["HTTP_Authorization"] = f"Bearer {settings.IDL_TOKEN}"

    def test_get_without_idl_token(self):
        """
        Test without the IDL token.

        We want to make sure that we get an 403_FORBIDDEN status code.
        """
        del self.client.defaults["HTTP_Authorization"]
        response = self.client.get(reverse("user-account-list"))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_get_empty(self):
        """Test the get method when there is no user account."""
        self.assertEqual(UserAccount.objects.count(), 0)

        response = self.client.get(reverse("user-account-list"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_no_exposed_user(self):
        """Test the get method for only non exposed users."""
        self.assertEqual(UserAccount.objects.count(), 0)
        for i in range(10):
            UserAccount.objects.create(
                username=f"User{i+1}@somewhere.net", expose=False
            )

        response = self.client.get(reverse("user-account-list"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json(), [])

    def test_get_exposed_user_without_groups(self):
        """Test the get method for only exposed users without any group."""
        self.assertEqual(UserAccount.objects.count(), 0)
        for i in range(10):
            UserAccount.objects.create(username=f"User{i+1}@somewhere.net", expose=True)

        response = self.client.get(reverse("user-account-list"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json(), [])

    def test_get_exposed_user_without_exposed_groups(self):
        """Test the get method for only exposed users without any exposed group."""
        self.assertEqual(UserAccount.objects.count(), 0)
        new_group = PermissionGroup.objects.create(
            name="group", description="", expose=False
        )
        for i in range(10):
            user = UserAccount.objects.create(
                username=f"User{i+1}@somewhere.net", expose=True
            )
            new_group.members.add(user)
            new_group.save()

        response = self.client.get(reverse("user-account-list"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json(), [])

    def test_get_exposed_user_with_exposed_group(self):
        """Test the get method for exposed users with exposed group."""
        self.assertEqual(UserAccount.objects.count(), 0)
        new_group = PermissionGroup.objects.create(
            name="group", description="", expose=True
        )
        aaa_user = UserAccount.objects.create(
            username="aaa@somewhere.net", displayname="A. AA", expose=True
        )
        bbb_user = UserAccount.objects.create(
            username="bbb@somewhere.net", displayname="Boris B.", expose=True
        )
        new_group.members.add(aaa_user)
        new_group.admins.add(bbb_user)
        new_group.save()

        response = self.client.get(reverse("user-account-list"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        expected = [
            {
                "id": str(aaa_user.id),
                "userName": "aaa@somewhere.net",
                "displayName": "A. AA",
                "memberedPermissionGroups": [str(new_group.id)],
                "administratedPermissionGroups": [],
            },
            {
                "id": str(bbb_user.id),
                "userName": "bbb@somewhere.net",
                "displayName": "Boris B.",
                "memberedPermissionGroups": [],
                "administratedPermissionGroups": [str(new_group.id)],
            },
        ]

        self.assertEqual(response.json(), expected)

    def test_get_search(self):
        """
        Test the search parameter, to filter some texts.

        The search parameter is a bit complex.
        It should run case insensitive & search on different fieldss with
        multiple keywords split by whitespace.
        """
        new_group = PermissionGroup.objects.create(
            name="group", description="", expose=True
        )
        aaa_user = UserAccount.objects.create(
            username="aaa@somewhere.net", displayname="A. AA", expose=True
        )
        bbb_user = UserAccount.objects.create(
            username="bbb@somewhere.net", displayname="Boris B.", expose=True
        )
        new_group.members.add(aaa_user)
        new_group.admins.add(bbb_user)
        new_group.save()

        response = self.client.get(reverse("user-account-list"), {"search": "AAA some"})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        expected = [
            {
                "id": str(aaa_user.id),
                "userName": "aaa@somewhere.net",
                "displayName": "A. AA",
                "memberedPermissionGroups": [str(new_group.id)],
                "administratedPermissionGroups": [],
            },
        ]

        self.assertEqual(response.json(), expected)

    def test_pagination(self):
        """Test the pagination."""
        new_group = PermissionGroup.objects.create(
            name="group", description="", expose=True
        )
        default_number_of_items_per_page = 100
        for i in range(default_number_of_items_per_page + 1):
            number = str(i + 1).zfill(3)
            user = UserAccount.objects.create(
                username=f"User {number}",
                displayname=f"U. S. Er {number}",
                expose=True,
            )
            new_group.members.add(user)
        new_group.save()

        response_default_pagination = self.client.get(reverse("user-account-list"))
        self.assertEqual(response_default_pagination.status_code, status.HTTP_200_OK)
        response_default_pagination_data = response_default_pagination.json()
        self.assertEqual(
            len(response_default_pagination_data), default_number_of_items_per_page
        )
        self.assertEqual(response_default_pagination_data[0]["userName"], "User 001")
        self.assertEqual(response_default_pagination_data[-1]["userName"], "User 100")

        response_page2 = self.client.get(reverse("user-account-list"), {"page": 2})
        self.assertEqual(response_page2.status_code, status.HTTP_200_OK)
        response_page2_data = response_page2.json()
        self.assertEqual(len(response_page2_data), 1)
        self.assertEqual(response_page2_data[0]["userName"], "User 101")

        response_no_pagination = self.client.get(
            reverse("user-account-list"), {"pagination": "false"}
        )
        self.assertEqual(response_no_pagination.status_code, status.HTTP_200_OK)
        response_no_pagination_data = response_no_pagination.json()
        self.assertEqual(
            len(response_no_pagination_data), default_number_of_items_per_page + 1
        )
        self.assertEqual(response_no_pagination_data[0]["userName"], "User 001")
        self.assertEqual(response_no_pagination_data[-1]["userName"], "User 101")

        response_custom_pagination = self.client.get(
            reverse("user-account-list"), {"itemsPerPage": 10, "page": 2}
        )
        self.assertEqual(response_custom_pagination.status_code, status.HTTP_200_OK)
        response_custom_pagination_data = response_custom_pagination.json()
        self.assertEqual(len(response_custom_pagination_data), 10)
        self.assertEqual(response_custom_pagination_data[0]["userName"], "User 011")
        self.assertEqual(response_custom_pagination_data[-1]["userName"], "User 020")

    def test_get_username_is(self):
        """Test the get method with the userName_is filter."""
        self.assertEqual(UserAccount.objects.count(), 0)
        new_group = PermissionGroup.objects.create(
            name="group", description="", expose=True
        )
        aaa_user = UserAccount.objects.create(
            username="aaa@somewhere.net", displayname="A. AA", expose=True
        )
        bbb_user = UserAccount.objects.create(
            username="bbb@somewhere.net", displayname="Boris B.", expose=True
        )
        new_group.members.add(aaa_user)
        new_group.admins.add(bbb_user)
        new_group.save()

        response = self.client.get(
            reverse("user-account-list"), {"userName_is": "bbb@somewhere.net"}
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        expected = [
            {
                "id": str(bbb_user.id),
                "userName": "bbb@somewhere.net",
                "displayName": "Boris B.",
                "memberedPermissionGroups": [],
                "administratedPermissionGroups": [str(new_group.id)],
            }
        ]

        self.assertEqual(response.json(), expected)

        response_not_found = self.client.get(
            reverse("user-account-list"), {"userName_is": "ccc@somewhere.net"}
        )
        self.assertEqual(response_not_found.status_code, status.HTTP_200_OK)
        self.assertEqual(response_not_found.json(), [])

    def test_get_id_is(self):
        """Test the get method with the id_is filter."""
        self.assertEqual(UserAccount.objects.count(), 0)
        new_group = PermissionGroup.objects.create(
            name="group", description="", expose=True
        )
        aaa_user = UserAccount.objects.create(
            username="aaa@somewhere.net", displayname="A. AA", expose=True
        )
        bbb_user = UserAccount.objects.create(
            username="bbb@somewhere.net", displayname="Boris B.", expose=True
        )
        new_group.members.add(aaa_user)
        new_group.admins.add(bbb_user)
        new_group.save()

        response = self.client.get(
            reverse("user-account-list"), {"id_is": str(bbb_user.id)}
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        expected = [
            {
                "id": str(bbb_user.id),
                "userName": "bbb@somewhere.net",
                "displayName": "Boris B.",
                "memberedPermissionGroups": [],
                "administratedPermissionGroups": [str(new_group.id)],
            }
        ]

        self.assertEqual(response.json(), expected)

        response_not_found = self.client.get(
            reverse("user-account-list"), {"id_is": "-1234"}
        )
        self.assertEqual(response_not_found.status_code, status.HTTP_200_OK)
        self.assertEqual(response_not_found.json(), [])

    def test_get_id_is_array(self):
        """Test the get method with the id_is[] array filter."""
        self.assertEqual(UserAccount.objects.count(), 0)
        new_group = PermissionGroup.objects.create(
            name="group", description="", expose=True
        )
        aaa_user = UserAccount.objects.create(
            username="aaa@somewhere.net", displayname="A. AA", expose=True
        )
        bbb_user = UserAccount.objects.create(
            username="bbb@somewhere.net", displayname="Boris B.", expose=True
        )
        new_group.members.add(aaa_user)
        new_group.admins.add(bbb_user)
        new_group.save()

        response = self.client.get(
            reverse("user-account-list"), {"id_is[]": [str(bbb_user.id), str(-9999)]}
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        expected = [
            {
                "id": str(bbb_user.id),
                "userName": "bbb@somewhere.net",
                "displayName": "Boris B.",
                "memberedPermissionGroups": [],
                "administratedPermissionGroups": [str(new_group.id)],
            }
        ]

        self.assertEqual(response.json(), expected)

        response_not_found = self.client.get(
            reverse("user-account-list"), {"id_is[]": ["-1234"]}
        )
        self.assertEqual(response_not_found.status_code, status.HTTP_200_OK)
        self.assertEqual(response_not_found.json(), [])

        response_both = self.client.get(
            reverse("user-account-list"),
            {"id_is[]": [str(aaa_user.id), str(bbb_user.id)]},
        )
        self.assertEqual(response_both.status_code, status.HTTP_200_OK)
        expected = [
            {
                "id": str(aaa_user.id),
                "userName": "aaa@somewhere.net",
                "displayName": "A. AA",
                "memberedPermissionGroups": [str(new_group.id)],
                "administratedPermissionGroups": [],
            },
            {
                "id": str(bbb_user.id),
                "userName": "bbb@somewhere.net",
                "displayName": "Boris B.",
                "memberedPermissionGroups": [],
                "administratedPermissionGroups": [str(new_group.id)],
            },
        ]

        self.assertEqual(response_both.json(), expected)


class TestIdlApiUserAccountDetailView(TestCase):
    """Tests for the user account detail view."""

    def setUp(self):
        """
        Startup our tests.

        We want all of our tests here to use the IDL token.
        """
        self.client.defaults["HTTP_Authorization"] = f"Bearer {settings.IDL_TOKEN}"

    def test_get_without_idl_token(self):
        """
        Test without the IDL token.

        We want to make sure that we get an 403_FORBIDDEN status code.
        And we want to make sure that the 403 is before
        we would return an 404 (as users without the permissions
        could check if there is a resource - even if they can't
        access them).
        """
        del self.client.defaults["HTTP_Authorization"]
        response = self.client.get(reverse("user-account-detail", args=(123,)))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_get_non_existing_user(self):
        """Test the get method when there is no user."""
        self.assertEqual(UserAccount.objects.count(), 0)

        response = self.client.get(reverse("user-account-detail", args=(123,)))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_get_not_exposed_user(self):
        """Test the get method for a user that is not exposed."""
        user = UserAccount.objects.create(
            username="aaa@somewhere.net", displayname="A. AA", expose=False
        )
        response = self.client.get(reverse("user-account-detail", args=(user.id,)))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_get_exposed_user_without_groups(self):
        """Test the get method for a user that has no groups."""
        user = UserAccount.objects.create(
            username="aaa@somewhere.net", displayname="A. AA", expose=True
        )
        response = self.client.get(reverse("user-account-detail", args=(user.id,)))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_get_exposed_user_without_exposed_groups(self):
        """Test the get method for a user that has no exposed groups."""
        group = PermissionGroup.objects.create(
            name="group", description="", expose=False
        )
        user = UserAccount.objects.create(
            username="aaa@somewhere.net", displayname="A. AA", expose=True
        )
        group.members.add(user)
        group.save()
        response = self.client.get(reverse("user-account-detail", args=(user.id,)))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_get_exposed_user_without_exposed_groups_for_admin(self):
        """Test the get method for a user that has no exposed groups (admin)."""
        group = PermissionGroup.objects.create(
            name="group", description="", expose=False
        )
        user = UserAccount.objects.create(
            username="aaa@somewhere.net", displayname="A. AA", expose=True
        )
        group.admins.add(user)
        group.save()
        response = self.client.get(reverse("user-account-detail", args=(user.id,)))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_get_exposed_user_with_exposed_groups(self):
        """Test the get method for a user that has exposed groups."""
        group = PermissionGroup.objects.create(
            name="group", description="", expose=True
        )
        user = UserAccount.objects.create(
            username="aaa@somewhere.net", displayname="A. AA", expose=True
        )
        group.members.add(user)
        group.save()
        response = self.client.get(reverse("user-account-detail", args=(user.id,)))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        expected = {
            "id": str(user.id),
            "userName": "aaa@somewhere.net",
            "displayName": "A. AA",
            "memberedPermissionGroups": [str(group.id)],
            "administratedPermissionGroups": [],
        }
        self.assertEqual(response.json(), expected)

    def test_get_exposed_user_with_exposed_groups_admin(self):
        """Test the get method for a user that has exposed groups (admin)."""
        group = PermissionGroup.objects.create(
            name="group", description="", expose=True
        )
        user = UserAccount.objects.create(
            username="aaa@somewhere.net", displayname="A. AA", expose=True
        )
        group.admins.add(user)
        group.save()
        response = self.client.get(reverse("user-account-detail", args=(user.id,)))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        expected = {
            "id": str(user.id),
            "userName": "aaa@somewhere.net",
            "displayName": "A. AA",
            "memberedPermissionGroups": [],
            "administratedPermissionGroups": [str(group.id)],
        }
        self.assertEqual(response.json(), expected)
