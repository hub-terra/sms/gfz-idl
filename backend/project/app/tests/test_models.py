# SPDX-FileCopyrightText: 2021 - 2022
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""Testcases for the django models."""

from django.db.utils import IntegrityError
from django.test import TestCase

from ..models import PermissionGroup, UserAccount


class TestPermissionGroup(TestCase):
    """Tests for the permission groups."""

    def test_str(self):
        """Test the __str__ method."""
        group = PermissionGroup(name="abc")
        as_str = str(group)
        self.assertEqual(as_str, "abc")

    def test_ordering(self):
        """Test the ordering."""
        self.assertEqual(PermissionGroup.objects.count(), 0)

        PermissionGroup.objects.create(name="fff", description="", expose=True)
        PermissionGroup.objects.create(name="mmm", description="", expose=True)
        PermissionGroup.objects.create(name="aaa", description="", expose=True)

        names = [x.name for x in PermissionGroup.objects.all()]

        self.assertEqual(names, ["aaa", "fff", "mmm"])

    def test_unique_name_and_src(self):
        """Test the unique constraint for the name & src."""
        PermissionGroup.objects.create(
            name="fff", description="", expose=True, src="abc"
        )
        with self.assertRaises(IntegrityError):
            PermissionGroup.objects.create(
                name="fff", description="", expose=True, src="abc"
            )


class TestUserAccount(TestCase):
    """Tests for the user accounts."""

    def test_str(self):
        """Test the __str__ method."""
        user = UserAccount(username="abc@gfz-potsdam.de")
        as_str = str(user)
        self.assertEqual(as_str, "abc@gfz-potsdam.de")

    def test_ordering(self):
        """Test the ordering."""
        self.assertEqual(UserAccount.objects.count(), 0)

        UserAccount.objects.create(username="fff@web", expose=True)
        UserAccount.objects.create(username="mmm@web", expose=True)
        UserAccount.objects.create(username="aaa@web", expose=True)

        names = [x.username for x in UserAccount.objects.all()]

        self.assertEqual(names, ["aaa@web", "fff@web", "mmm@web"])

    def test_unique_username(self):
        """Test the unique constraint for the username."""
        UserAccount.objects.create(username="fff@web", expose=True)
        with self.assertRaises(IntegrityError):
            UserAccount.objects.create(username="fff@web", expose=True)
