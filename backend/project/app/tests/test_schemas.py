# SPDX-FileCopyrightText: 2021 - 2022
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""Tests for the schemas."""

from django.test import TestCase

from ..models import PermissionGroup, UserAccount
from ..schemas import PermissionGroupSchema, UserAccountSchema


class TestPermissionGroupSchema(TestCase):
    """Tests for the permission group schema."""

    def test_serialize_with_all(self):
        """Test the serialize with all users."""
        u1 = UserAccount.objects.create(username="u1", expose=False)
        u2 = UserAccount.objects.create(username="u2", expose=True)
        u3 = UserAccount.objects.create(username="u3", expose=False)

        g1 = PermissionGroup.objects.create(
            name="g1",
            description="abc",
        )
        g1.members.add(u1)
        g1.members.add(u2)
        g1.admins.add(u2)
        g1.admins.add(u3)
        g1.save()

        schema = PermissionGroupSchema(only_exposed_users=False)

        result = schema.serialize_instance_to_dict(g1)
        self.assertEqual(
            result,
            {
                "id": str(g1.id),
                "name": "g1",
                "description": "abc",
                "members": [str(u1.id), str(u2.id)],
                "admins": [str(u2.id), str(u3.id)],
            },
        )

    def test_serialize_only_exposed_users(self):
        """Test the serialize that should only use exposed users."""
        u1 = UserAccount.objects.create(username="u1", expose=False)
        u2 = UserAccount.objects.create(username="u2", expose=True)
        u3 = UserAccount.objects.create(username="u3", expose=False)

        g1 = PermissionGroup.objects.create(
            name="g1",
            description="abc",
        )
        g1.members.add(u1)
        g1.members.add(u2)
        g1.admins.add(u2)
        g1.admins.add(u3)
        g1.save()

        schema = PermissionGroupSchema(only_exposed_users=True)

        result = schema.serialize_instance_to_dict(g1)
        self.assertEqual(
            result,
            {
                "id": str(g1.id),
                "name": "g1",
                "description": "abc",
                "members": [str(u2.id)],
                "admins": [str(u2.id)],
            },
        )


class TestUserAcocuntSchema(TestCase):
    """Tests for the UserAccountSchema."""

    def test_serialize_with_all(self):
        """Test the serialize with all groups."""
        u1 = UserAccount.objects.create(
            username="u1", displayname="user 1", expose=False
        )

        g1 = PermissionGroup.objects.create(name="g1", description="abc", expose=True)
        g1.members.add(u1)
        g1.admins.add(u1)
        g1.save()
        g2 = PermissionGroup.objects.create(name="g2", description="abc", expose=False)
        g2.members.add(u1)
        g2.admins.add(u1)
        g2.save()
        g3 = PermissionGroup.objects.create(name="g3", description="abc", expose=False)
        g3.members.add(u1)
        g3.save()

        schema = UserAccountSchema(only_exposed_groups=False)
        result = schema.serialize_instance_to_dict(u1)
        self.assertEqual(
            result,
            {
                "id": str(u1.id),
                "userName": "u1",
                "displayName": "user 1",
                "memberedPermissionGroups": [str(g1.id), str(g2.id), str(g3.id)],
                "administratedPermissionGroups": [str(g1.id), str(g2.id)],
            },
        )

    def test_serialize_only_exposed_groups(self):
        """Test the serialize that should only use exposed groups."""
        u1 = UserAccount.objects.create(
            username="u1", displayname="user 1", expose=False
        )

        g1 = PermissionGroup.objects.create(name="g1", description="abc", expose=True)
        g1.members.add(u1)
        g1.admins.add(u1)
        g1.save()
        g2 = PermissionGroup.objects.create(name="g2", description="abc", expose=False)
        g2.members.add(u1)
        g2.admins.add(u1)
        g2.save()
        g3 = PermissionGroup.objects.create(name="g3", description="abc", expose=False)
        g3.members.add(u1)
        g3.save()

        schema = UserAccountSchema(only_exposed_groups=True)
        result = schema.serialize_instance_to_dict(u1)
        self.assertEqual(
            result,
            {
                "id": str(u1.id),
                "userName": "u1",
                "displayName": "user 1",
                "memberedPermissionGroups": [str(g1.id)],
                "administratedPermissionGroups": [str(g1.id)],
            },
        )
