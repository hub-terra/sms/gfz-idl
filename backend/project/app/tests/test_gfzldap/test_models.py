# SPDX-FileCopyrightText: 2021
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""Tests for the ldap models."""

from django.test import TestCase

from ...gfzldap.models import Group, Person


class TestGroup(TestCase):
    """Tests for the Group model."""

    def test_get_description_with_group_display_name_en(self):
        """Test the case that we have the gfz_group_display_name_en."""
        test_group = Group(
            gfz_group_display_name_en="abc",
        )
        description = test_group.get_description()
        self.assertEqual(description, "abc")

    def test_get_description_with_group_display_name_de(self):
        """Test one fallback to use the gfz_group_display_name_de."""
        test_group = Group(
            gfz_group_display_name_en=None,
            gfz_group_display_name_de="def",
        )
        description = test_group.get_description()
        self.assertEqual(description, "def")

    def test_get_description_with_group_en(self):
        """Test one fallback to use the gfz_group_en."""
        test_group = Group(
            gfz_group_display_name_en=None,
            gfz_group_display_name_de=None,
            gfz_group_en="ghi",
        )
        description = test_group.get_description()
        self.assertEqual(description, "ghi")

    def test_get_description_with_group_de(self):
        """Test one fallback to use the gfz_group_de."""
        test_group = Group(
            gfz_group_display_name_en=None,
            gfz_group_display_name_de=None,
            gfz_group_en=None,
            gfz_group_de="jkl",
        )
        description = test_group.get_description()
        self.assertEqual(description, "jkl")

    def test_get_description_with_cn(self):
        """Test one fallback to use the cn."""
        test_group = Group(
            gfz_group_display_name_en=None,
            gfz_group_display_name_de=None,
            gfz_group_en=None,
            gfz_group_de=None,
            cn="mno",
        )
        description = test_group.get_description()
        self.assertEqual(description, "mno")

    def test_is_visible_normal(self):
        """Test with a normal readable group."""
        test_group = Group(
            gfz_group_readable=True, gfz_group_hide=False, gfz_group_disabled=False
        )
        result = test_group.is_visible()
        self.assertTrue(result)

    def test_is_visible_not_readable(self):
        """Test with a not readable group."""
        test_group = Group(
            gfz_group_readable=False, gfz_group_hide=False, gfz_group_disabled=False
        )
        result = test_group.is_visible()
        self.assertFalse(result)

    def test_is_visible_hidden(self):
        """Test with a hidden group."""
        test_group = Group(
            gfz_group_readable=True, gfz_group_hide=True, gfz_group_disabled=False
        )
        result = test_group.is_visible()
        self.assertFalse(result)

    def test_is_visible_disabled(self):
        """Test with a disabled group."""
        test_group = Group(
            gfz_group_readable=True,
            gfz_group_hide=False,
            gfz_group_disabled=True,
        )
        result = test_group.is_visible()
        self.assertFalse(result)


class TestPerson(TestCase):
    """Test cases for the Persons."""

    def test_repr(self):
        """Test the __repr__ method."""
        p = Person(uid="aaa")
        self.assertEqual(repr(p), "Person(uid='aaa')")
