# SPDX-FileCopyrightText: 2021
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""Tests for the filter functions."""

from django.test import TestCase

from ...gfzldap import filters


class TestIsNullOrEmpty(TestCase):
    """Test the is_null_or_empty function."""

    def test_null_true(self):
        """Test with None for values that should be None."""
        self.assertTrue(filters.is_null_or_empty(None, True))

    def test_null_false(self):
        """Test with None for values that should not be None."""
        self.assertFalse(filters.is_null_or_empty(None, False))

    def test_empty_str_true(self):
        """Test with empty string for values that should be empty."""
        self.assertTrue(filters.is_null_or_empty("", True))

    def test_empty_str_false(self):
        """Test with empty string for values that should not be empty."""
        self.assertFalse(filters.is_null_or_empty("", False))

    def test_empty_list_true(self):
        """Test with empty list for values that should be empty."""
        self.assertTrue(filters.is_null_or_empty([], True))

    def test_empty_list_false(self):
        """Test with empty list for values that should not be empty."""
        self.assertFalse(filters.is_null_or_empty([], False))

    def test_str_true(self):
        """Test with a string for values that should be empty."""
        self.assertFalse(filters.is_null_or_empty("foo", True))

    def test_str_false(self):
        """Test with a string for values that should not be empty."""
        self.assertTrue(filters.is_null_or_empty("foo", False))


class TestEqual(TestCase):
    """Test the equal function."""

    def test_equal_str(self):
        """Test with values that should be equal."""
        self.assertTrue(filters.equals("a", "a"))

    def test_non_equal_str(self):
        """Test with values that should not be equal."""
        self.assertFalse(filters.equals("a", "b"))


class TestContains(TestCase):
    """Test the contains function."""

    def test_str_included(self):
        """Test with a string that is part of the filter values."""
        self.assertTrue(filters.contains("a", ["a", "b"]))

    def test_str_not_included(self):
        """Test with a string that is not part of the filter values."""
        self.assertFalse(filters.contains("a", ["c", "b"]))

    def test_str_empty_list(self):
        """Test with an empty list of filter values."""
        self.assertFalse(filters.contains("a", []))


class TestOneOfTheValuesEquals(TestCase):
    """Tests for the one_of_the_values_equals function."""

    def test_str_included(self):
        """Test with a list that has the wanted value."""
        self.assertTrue(filters.one_of_the_values_equals(["a", "b"], "b"))

    def test_str_not_included(self):
        """Test with a list that doesn't have the value."""
        self.assertFalse(filters.one_of_the_values_equals(["a", "c"], "b"))

    def test_str_empty_list(self):
        """Test with an empyt list."""
        self.assertFalse(filters.one_of_the_values_equals([], "a"))


class TestListContainsOne(TestCase):
    """Tests for the list_contains_one function."""

    def test_included(self):
        """Test with an overlapping of the values."""
        self.assertTrue(filters.list_contains_one(["a", "b"], ["b"]))

    def test_not_included(self):
        """Test with no overlapping values."""
        self.assertFalse(filters.list_contains_one(["a", "c"], ["b"]))

    def test_empty_list(self):
        """Test with empty lists."""
        self.assertFalse(filters.list_contains_one([], ["a"]))
        self.assertFalse(filters.list_contains_one(["a"], []))
        self.assertFalse(filters.list_contains_one([], []))


class TestStartswith(TestCase):
    """Test the startswith filter function."""

    def test_str_true(self):
        """Test with a prefix that is in the value."""
        self.assertTrue(filters.startswith("abc", "ab"))

    def test_str_false(self):
        """Test with a prefix that is not in the value."""
        self.assertFalse(filters.startswith("abc", "eab"))

    def test_none_false(self):
        """Test with a None value."""
        self.assertFalse(filters.startswith(None, "eab"))
