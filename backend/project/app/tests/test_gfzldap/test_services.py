# SPDX-FileCopyrightText: 2021 - 2022
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""Test cases for the services functions."""
from unittest.mock import patch

from django.test import TestCase

from ...gfzldap import services
from ...gfzldap.models import Group, Person
from ...models import PermissionGroup, UserAccount


class TestGetGroupNames(TestCase):
    """Tests for the get_group_names function."""

    def test_input(self):
        """Test that we can extract the group names from a group ldap query."""
        with patch.object(Group.objects, "raw_query_all") as mock:
            mock.return_value = [{"cn": [b"name1"]}, {"cn": [b"name2"]}]
            group_names = services.get_group_names()
        self.assertEqual(group_names, ["name1", "name2"])

    def test_sorted(self):
        """Test that the results are sorted."""
        with patch.object(Group.objects, "raw_query_all") as mock:
            mock.return_value = [{"cn": [b"name2"]}, {"cn": [b"name1"]}]
            group_names = services.get_group_names()
        self.assertEqual(group_names, ["name1", "name2"])

    def test_empty(self):
        """Test if the ldap query was empty."""
        with patch.object(Group.objects, "raw_query_all") as mock:
            mock.return_value = []
            group_names = services.get_group_names()
        self.assertEqual(group_names, [])


class TestGetPersonNames(TestCase):
    """Test the get_person_names function."""

    def test_input(self):
        """Test that we can extract the person names from a person ldap query."""
        with patch.object(Person.objects, "raw_query_all") as mock:
            mock.return_value = [{"uid": [b"name1"]}, {"uid": [b"name2"]}]
            person_names = services.get_person_names()
        self.assertEqual(person_names, ["name1", "name2"])

    def test_sorted(self):
        """Test that the results are sorted."""
        with patch.object(Person.objects, "raw_query_all") as mock:
            mock.return_value = [{"uid": [b"name2"]}, {"uid": [b"name1"]}]
            person_names = services.get_person_names()
        self.assertEqual(person_names, ["name1", "name2"])

    def test_empty(self):
        """Test if the ldap query was empty."""
        with patch.object(Person.objects, "raw_query_all") as mock:
            mock.return_value = []
            person_names = services.get_person_names()
        self.assertEqual(person_names, [])


class TestGetRawGroupOrNone(TestCase):
    """Tests for the get_raw_group_or_none function."""

    def test_input(self):
        """Test with normal input."""
        mocked_group = {
            "cn": ["name1"],
            "gfzGroupAdmin": [b"uid=abc,ou=People,dc=gfz-potsdam.de"],
        }
        with patch.object(Group.objects, "raw_query_one_or_none") as mock:
            mock.return_value = mocked_group
            raw_group = services.get_raw_group_or_none("name1")

        self.assertEqual(raw_group, mocked_group)

    def test_empty(self):
        """Test the case that the ldap result is empty."""
        with patch.object(Group.objects, "raw_query_one_or_none") as mock:
            mock.return_value = None
            raw_group = services.get_raw_group_or_none("name1")

        self.assertIsNone(raw_group)


class TestGetRawPersonOrNone(TestCase):
    """Tests for the get_raw_person_or_none function."""

    def test_input(self):
        """Test with normal input."""
        mocked_person = {"uid": ["abc"], "gfzSalutation": [b"Herr"]}
        with patch.object(Person.objects, "raw_query_one_or_none") as mock:
            mock.return_value = mocked_person
            raw_person = services.get_raw_person_or_none("name1")
        self.assertEqual(raw_person, mocked_person)

    def test_empty(self):
        """Test for the case that the ldap result is empty."""
        with patch.object(Person.objects, "raw_query_one_or_none") as mock:
            mock.return_value = None
            raw_person = services.get_raw_person_or_none("name1")
        self.assertIsNone(raw_person)


class TestSync(TestCase):
    """Tests for the sync function - and its side-effects."""

    def test_empty_db_empty_ldap(self):
        """
        Test with an empty db and an empty answer from the ldap.

        When starting with an empty db and an empty ldap, we should
        stay with an empty db.
        """
        self.assertEqual(PermissionGroup.objects.count(), 0)
        self.assertEqual(UserAccount.objects.count(), 0)

        with patch.object(services, "query_ldap_for_groups_and_people") as mock:
            mock.return_value = services.LdapQueryResultForGroupsAndPeople(
                groups=[],
                people=[],
            )

            services.sync()

        self.assertEqual(PermissionGroup.objects.count(), 0)
        self.assertEqual(UserAccount.objects.count(), 0)

    def test_insert_one_ldap_group(self):
        """Test to import one ldap group."""
        self.assertEqual(PermissionGroup.objects.count(), 0)
        self.assertEqual(UserAccount.objects.count(), 0)

        with patch.object(services, "query_ldap_for_groups_and_people") as mock:
            mock.return_value = services.LdapQueryResultForGroupsAndPeople(
                groups=[
                    Group(
                        cn="abc",
                        gfz_group_display_name_en="desc",
                        gfz_group_readable=True,
                        gfz_group_hide=False,
                        gfz_group_disabled=False,
                        unique_members=[],
                        gfz_group_admins=[],
                        mgrp_rfc822_mail_member=[],
                    ),
                ],
                people=[],
            )

            services.sync()
        self.assertEqual(PermissionGroup.objects.count(), 1)

        new_group = PermissionGroup.objects.all().first()
        self.assertIsNotNone(new_group)
        self.assertEqual(new_group.name, "abc")
        self.assertEqual(new_group.description, "desc")
        # As long as we can't extract a sms/datahub attribute from the
        # ldap group, we need to start not to expose the groups.
        self.assertEqual(new_group.expose, False)

    def test_update_one_ldap_group_stay_with_expose(self):
        """
        Test to update one ldap group.

        In case we expose this group already, we want to stay with
        the expose value.
        """
        self.assertEqual(PermissionGroup.objects.count(), 0)
        self.assertEqual(UserAccount.objects.count(), 0)

        PermissionGroup.objects.create(
            name="abc", description="Old description", expose=True
        )

        with patch.object(services, "query_ldap_for_groups_and_people") as mock:
            mock.return_value = services.LdapQueryResultForGroupsAndPeople(
                groups=[
                    Group(
                        cn="abc",
                        gfz_group_display_name_en="desc",
                        gfz_group_readable=True,
                        gfz_group_hide=False,
                        gfz_group_disabled=False,
                        unique_members=[],
                        gfz_group_admins=[],
                        mgrp_rfc822_mail_member=[],
                    ),
                ],
                people=[],
            )

            services.sync()
        self.assertEqual(PermissionGroup.objects.count(), 1)

        new_group = PermissionGroup.objects.all().first()
        self.assertIsNotNone(new_group)
        self.assertEqual(new_group.name, "abc")
        self.assertEqual(new_group.description, "desc")
        # For the moment we don't overwrite an existing expose
        # if we can stay with it.
        self.assertEqual(new_group.expose, True)

    def test_update_one_ldap_group_remove_expose(self):
        """
        Test to update one ldap group.

        In case we expose this group already, but the current
        update tells us that the group is no longer readable
        (or it is now hidden or disabled) then we don't
        want to stay with the expose anymore.
        """
        self.assertEqual(PermissionGroup.objects.count(), 0)
        self.assertEqual(UserAccount.objects.count(), 0)

        PermissionGroup.objects.create(
            name="abc", description="Old description", expose=True
        )

        with patch.object(services, "query_ldap_for_groups_and_people") as mock:
            mock.return_value = services.LdapQueryResultForGroupsAndPeople(
                groups=[
                    Group(
                        cn="abc",
                        gfz_group_display_name_en="desc",
                        gfz_group_readable=False,
                        gfz_group_hide=False,
                        gfz_group_disabled=False,
                        unique_members=[],
                        gfz_group_admins=[],
                        mgrp_rfc822_mail_member=[],
                    ),
                ],
                people=[],
            )

            services.sync()
        self.assertEqual(PermissionGroup.objects.count(), 1)

        new_group = PermissionGroup.objects.all().first()
        self.assertIsNotNone(new_group)
        self.assertEqual(new_group.name, "abc")
        # For the moment we don't overwrite an existing expose
        # if we can stay with it.
        self.assertEqual(new_group.expose, False)

    def test_insert_one_ldap_person(self):
        """Test to import one ldap person."""
        self.assertEqual(PermissionGroup.objects.count(), 0)
        self.assertEqual(UserAccount.objects.count(), 0)

        with patch.object(services, "query_ldap_for_groups_and_people") as mock:
            mock.return_value = services.LdapQueryResultForGroupsAndPeople(
                groups=[],
                people=[
                    Person(
                        mail="abc@gfz-potsdam.de",
                        gfz_account_disabled=False,
                    )
                ],
            )

            services.sync()
        self.assertEqual(UserAccount.objects.count(), 1)

        new_user = UserAccount.objects.all().first()
        self.assertIsNotNone(new_user)
        self.assertEqual(new_user.username, "abc@gfz-potsdam.de")
        self.assertTrue(new_user.expose)

    def test_update_one_ldap_person(self):
        """Test to update an existing ldap person."""
        self.assertEqual(PermissionGroup.objects.count(), 0)
        self.assertEqual(UserAccount.objects.count(), 0)

        UserAccount.objects.create(
            username="abc@gfz-potsdam.de", displayname="abc@gfz-potsdam.de", expose=True
        )

        with patch.object(services, "query_ldap_for_groups_and_people") as mock:
            mock.return_value = services.LdapQueryResultForGroupsAndPeople(
                groups=[],
                people=[
                    Person(
                        mail="abc@gfz-potsdam.de",
                        gfz_account_disabled=True,
                    )
                ],
            )

            services.sync()
        self.assertEqual(UserAccount.objects.count(), 1)

        new_user = UserAccount.objects.all().first()
        self.assertIsNotNone(new_user)
        self.assertEqual(new_user.username, "abc@gfz-potsdam.de")
        self.assertFalse(new_user.expose)

    def test_member(self):
        """Test that we can add a normal member to a group."""
        self.assertEqual(PermissionGroup.objects.count(), 0)
        self.assertEqual(UserAccount.objects.count(), 0)

        with patch.object(services, "query_ldap_for_groups_and_people") as mock:
            mock.return_value = services.LdapQueryResultForGroupsAndPeople(
                groups=[
                    Group(
                        cn="abcgroup",
                        gfz_group_readable=True,
                        gfz_group_hide=False,
                        gfz_group_disabled=False,
                        gfz_group_admins=[],
                        unique_members=["uid=abc,key=value"],
                        mgrp_rfc822_mail_member=[],
                    )
                ],
                people=[
                    Person(
                        mail="abc@gfz-potsdam.de",
                        gfz_account_disabled=False,
                    )
                ],
            )

            services.sync()
        self.assertEqual(PermissionGroup.objects.count(), 1)
        self.assertEqual(UserAccount.objects.count(), 1)

        new_group = PermissionGroup.objects.first()
        new_user = UserAccount.objects.first()

        self.assertIn(new_user, new_group.members.all())

    def test_external_member(self):
        """Test that we can add a normal member to a group."""
        self.assertEqual(PermissionGroup.objects.count(), 0)
        self.assertEqual(UserAccount.objects.count(), 0)

        with patch.object(services, "query_ldap_for_groups_and_people") as mock:
            mock.return_value = services.LdapQueryResultForGroupsAndPeople(
                groups=[
                    Group(
                        cn="abcgroup",
                        gfz_group_readable=True,
                        gfz_group_hide=False,
                        gfz_group_disabled=False,
                        gfz_group_admins=[],
                        unique_members=[],
                        mgrp_rfc822_mail_member=["abc@ufz.de"],
                    )
                ],
                people=[],
            )

            services.sync()
        self.assertEqual(PermissionGroup.objects.count(), 1)
        self.assertEqual(UserAccount.objects.count(), 1)

        new_group = PermissionGroup.objects.first()
        new_user = UserAccount.objects.first()
        self.assertEqual(new_user.username, "abc@ufz.de")
        self.assertTrue(new_user.expose)

        self.assertIn(new_user, new_group.members.all())

    def test_admin(self):
        """Test that we can add an admin to a group."""
        self.assertEqual(PermissionGroup.objects.count(), 0)
        self.assertEqual(UserAccount.objects.count(), 0)

        with patch.object(services, "query_ldap_for_groups_and_people") as mock:
            mock.return_value = services.LdapQueryResultForGroupsAndPeople(
                groups=[
                    Group(
                        cn="abcgroup",
                        gfz_group_readable=True,
                        gfz_group_hide=False,
                        gfz_group_disabled=False,
                        gfz_group_admins=["uid=abc,key=value"],
                        unique_members=[],
                        mgrp_rfc822_mail_member=[],
                    )
                ],
                people=[
                    Person(
                        mail="abc@gfz-potsdam.de",
                        gfz_account_disabled=False,
                    )
                ],
            )

            services.sync()
        self.assertEqual(PermissionGroup.objects.count(), 1)
        self.assertEqual(UserAccount.objects.count(), 1)

        new_group = PermissionGroup.objects.first()
        new_user = UserAccount.objects.first()

        self.assertIn(new_user, new_group.admins.all())

    def test_owner(self):
        """Test that we can add an admin to a group via the owner field."""
        self.assertEqual(PermissionGroup.objects.count(), 0)
        self.assertEqual(UserAccount.objects.count(), 0)

        with patch.object(services, "query_ldap_for_groups_and_people") as mock:
            mock.return_value = services.LdapQueryResultForGroupsAndPeople(
                groups=[
                    Group(
                        cn="abcgroup",
                        gfz_group_readable=True,
                        gfz_group_hide=False,
                        gfz_group_disabled=False,
                        gfz_group_admins=[],
                        unique_members=[],
                        owner="uid=abc,key=value",
                        mgrp_rfc822_mail_member=[],
                    )
                ],
                people=[
                    Person(
                        mail="abc@gfz-potsdam.de",
                        gfz_account_disabled=False,
                    )
                ],
            )

            services.sync()
        self.assertEqual(PermissionGroup.objects.count(), 1)
        self.assertEqual(UserAccount.objects.count(), 1)

        new_group = PermissionGroup.objects.first()
        new_user = UserAccount.objects.first()

        self.assertIn(new_user, new_group.admins.all())
