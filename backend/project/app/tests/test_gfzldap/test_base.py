# SPDX-FileCopyrightText: 2021
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""Tests of the ldap base model, the fields and its object manager."""

from unittest.mock import patch

from django.test import TestCase

from ...gfzldap.base import LdapBaseModel
from ...gfzldap.exceptions import ModelNotFoundException
from ...gfzldap.fields import ArrayField, BooleanField, StringField


class DummyModel(LdapBaseModel):
    """
    Dummy ldap model.

    In order to test our base model - which is abstract - we
    need to create an implementation of the model.
    So here we have a dummy model with 3 fields.
    """

    aaa = StringField(source="aab", primary_key=True)
    bbb = BooleanField(default=False)
    ccc = ArrayField(base_field=StringField())

    class Meta:
        """Meta class for our dummy model."""

        query = "dummy"


class DummyConnection:
    """
    Stub to mock a connection.

    The stub is there to return a prefined
    set of outputs and to let a client know
    what the parameter for the search_s function
    were.

    I guess all of this can also be done
    with a mock - but this here seems for
    me a little bit more clear.
    """

    def __init__(self, return_value):
        """Init the object."""
        self.was_executed_with_query = None
        self.was_executed_with_filter = None
        self.return_value = return_value

    def search_s(self, query, scope, filter=None):
        """
        Run the search.

        This is meant as a replacement for the
        ldap.initialize(url).search_s(...) call.
        """
        self.was_executed_with_query = query
        self.was_executed_with_filter = filter

        return self.return_value


class TestBaseViaDummy(TestCase):
    """Test our base model by using the dummy model."""

    def test_pk_python_field(self):
        """
        Test the python pk field name.

        This is the one used in python for filters,
        not the one that we send to the ldap.
        """
        pk_field = DummyModel._get_pk_python_field()
        self.assertEqual(pk_field, "aaa")

    def test_pk_ldap_field(self):
        """
        Test the ldap pk field name.

        As this is the one we send to the ldap,
        it doesn't need to be the one we to access
        the field in python.
        The difference is mostly due to different
        naming conventions (gfz_field vs gfzField).
        """
        pk_field = DummyModel._get_pk_ldap_field()
        self.assertEqual(pk_field, "aab")

    def test_defaults(self):
        """Test the default values of the fields in the init method.."""
        d = DummyModel()
        self.assertIsNone(d.aaa)
        self.assertFalse(d.bbb)
        self.assertEqual(d.ccc, [])

    def test_equal(self):
        """Test the default implementation for the eq method."""
        d1 = DummyModel(aaa="aaa", bbb=True, ccc=["foo", "bar"])
        d2 = DummyModel(aaa="aaa", bbb=True, ccc=["foo", "bar"])
        d3 = DummyModel(aaa="aab", bbb=True, ccc=["foo", "bar"])
        d4 = DummyModel(aaa="aaa", bbb=False, ccc=["foo", "bar"])
        d5 = DummyModel(aaa="aaa", bbb=True, ccc=["foo"])

        self.assertTrue(d1 == d2)
        self.assertFalse(d1 == d3)
        self.assertFalse(d1 == d4)
        self.assertFalse(d1 == d5)

        class Dummy2Model(LdapBaseModel):
            """
            Second dummy model.

            The idea is to test with a differnt class with the
            very same structure.
            """

            aaa = StringField(source="aab", primary_key=True)
            bbb = BooleanField(default=False)
            ccc = ArrayField(base_field=StringField())

            class Meta:
                """Meta class for the alternativ model."""

                query = "dummy"

        dd1 = Dummy2Model(aaa="aaa", bbb=True, ccc=["foo", "bar"])
        # Same structure, same content but differnt class => should not be equal.
        self.assertFalse(d1 == dd1)

    def test_repr(self):
        """Test the default repr implementation."""
        d1 = DummyModel(aaa="a12", bbb=True, ccc=["foo", "bar"])
        self.assertEqual(repr(d1), "DummyModel(aaa='a12')")

    def test_from_ldap_dict(self):
        """Test filling the model from the ldap data."""
        ldap_dict = {"aab": [b"aaa"], "bbb": [b"TRUE"], "ccc": [b"foo", b"bar"]}
        d1 = DummyModel._from_ldap_dict(ldap_dict)
        d2 = DummyModel(aaa="aaa", bbb=True, ccc=["foo", "bar"])
        self.assertEqual(d1, d2)

    def test_get_fields(self):
        """
        Test the intropection to get the fields.

        The fields are used at many points, especially to provide
        default implementations for some methods.
        """
        fields = DummyModel._get_fields()
        expected = [
            ("aaa", StringField(source="aab", primary_key=True)),
            ("bbb", BooleanField(default=False)),
            ("ccc", ArrayField(base_field=StringField())),
        ]
        self.assertEqual(fields, expected)

    def test_get_filters(self):
        """
        Test some of the available filters.

        Filters are defined by the fields, and should follow
        mostly django model conventions: Using 2 underscores
        after the field name to check with serveral operations.
        """
        filters = DummyModel._get_filters()

        self.assertTrue("aaa" in filters.keys())
        self.assertTrue("bbb" in filters.keys())
        self.assertTrue("ccc" in filters.keys())

        self.assertTrue("aaa__null" in filters.keys())
        self.assertTrue("ccc__null" in filters.keys())

        self.assertTrue("aaa__in" in filters.keys())
        self.assertTrue("aaa__startswith" in filters.keys())
        self.assertTrue("ccc__in" in filters.keys())
        self.assertTrue("ccc__contains" in filters.keys())

        d1 = DummyModel(aaa="aaa", bbb=True, ccc=["foo", "bar"])
        d2 = DummyModel(aaa="aab", bbb=True, ccc=["foo", "bar"])

        self.assertTrue(filters["aaa"](d1, "aaa"))
        self.assertFalse(filters["aaa"](d2, "aaa"))

        self.assertTrue(filters["ccc__contains"](d1, "foo"))
        self.assertFalse(filters["ccc__contains"](d1, "blub"))

    def test_query_all(self):
        """Test to query all objects."""
        with patch.object(DummyModel.objects, "raw_query_all") as mock:
            mock.return_value = [
                {"aab": [b"a12"], "bbb": [b"TRUE"], "ccc": [b"foo", b"bar"]},
                {"aab": [b"a123"], "bbb": [b"FALSE"], "ccc": [b"muh", b"meh"]},
            ]
            all_dummies = DummyModel.objects.all()
            self.assertEqual(all_dummies.count(), 2)
            self.assertEqual(all_dummies[0].aaa, "a12")
            self.assertEqual(all_dummies[0].bbb, True)
            self.assertEqual(all_dummies[0].ccc, ["foo", "bar"])
            self.assertEqual(all_dummies[1].aaa, "a123")
            self.assertEqual(all_dummies[1].bbb, False)
            self.assertEqual(all_dummies[1].ccc, ["muh", "meh"])

    def test_query_filter(self):
        """Test the filter of the queryset."""
        with patch.object(DummyModel.objects, "raw_query_all") as mock:
            mock.return_value = [
                {"aab": [b"a12"], "bbb": [b"TRUE"], "ccc": [b"foo", b"bar"]},
                {"aab": [b"a123"], "bbb": [b"FALSE"], "ccc": [b"muh", b"meh"]},
            ]
            all_dummies = DummyModel.objects.filter(ccc__contains="foo")
            self.assertEqual(all_dummies.count(), 1)
            self.assertEqual(all_dummies[0].aaa, "a12")
            self.assertEqual(all_dummies[0].bbb, True)
            self.assertEqual(all_dummies[0].ccc, ["foo", "bar"])

    def test_query_exclude(self):
        """Test the exclude of the queryset."""
        with patch.object(DummyModel.objects, "raw_query_all") as mock:
            mock.return_value = [
                {"aab": [b"a12"], "bbb": [b"TRUE"], "ccc": [b"foo", b"bar"]},
                {"aab": [b"a123"], "bbb": [b"FALSE"], "ccc": [b"muh", b"meh"]},
            ]
            all_dummies = DummyModel.objects.exclude(ccc__contains="meh")
            self.assertEqual(all_dummies.count(), 1)
            self.assertEqual(all_dummies[0].aaa, "a12")
            self.assertEqual(all_dummies[0].bbb, True)
            self.assertEqual(all_dummies[0].ccc, ["foo", "bar"])

            self.assertIsNotNone(all_dummies.first())
            self.assertEqual(all_dummies.first().aaa, "a12")

            no_dummies = DummyModel.objects.exclude(aaa__startswith="a")
            self.assertEqual(no_dummies.count(), 0)
            self.assertEqual(no_dummies.first(), None)

    def test_meta_query(self):
        """Test the query text."""
        self.assertEqual(DummyModel.Meta.query, "dummy")

    def test_get(self):
        """Test the get to find one single entry."""
        with patch.object(DummyModel.objects, "raw_query_one_or_none") as mock:
            mock.return_value = {
                "aab": [b"a12"],
                "bbb": [b"TRUE"],
                "ccc": [b"foo", b"bar"],
            }
            dummy = DummyModel.objects.get("a12")
            self.assertEqual(dummy.aaa, "a12")
            self.assertEqual(dummy.bbb, True)
            self.assertEqual(dummy.ccc, ["foo", "bar"])

    def test_get_non_existing(self):
        """Test the get when there is no such entry (ModelNotFoundException)."""
        with patch.object(DummyModel.objects, "raw_query_one_or_none") as mock:
            mock.return_value = None

            def query_non_existing_dummy():
                """Run the code and raise an exception."""
                DummyModel.objects.get("a13")

            self.assertRaises(ModelNotFoundException, query_non_existing_dummy)

    def test_get_or_none_non_existing(self):
        """Test the get_or_none when there is no such entry (None)."""
        with patch.object(DummyModel.objects, "raw_query_one_or_none") as mock:
            mock.return_value = None
            self.assertIsNone(DummyModel.objects.get_or_none("a13"))

    def test_objects_instance(self):
        """
        Test the identity of the object manager access.

        This is mostly just a need for testing - so that we can mock
        the DummyModel.objects in one place, and the DummyModel.objects
        in another place (different import) is also effected by the mock.
        """
        i1 = DummyModel.objects
        i2 = DummyModel.objects
        self.assertTrue(i1 is i2)

    def test_raw_query_one_or_none(self):
        """Test the raw_query_one_or_none method."""
        with patch.object(DummyModel.objects, "connect") as mock:
            dummy_connection = DummyConnection([("query", {"aab": [b"foo"]})])
            mock.return_value = dummy_connection
            raw_query_result = DummyModel.objects.raw_query_one_or_none("foo")
            self.assertEqual(raw_query_result, {"aab": [b"foo"]})
            self.assertEqual(dummy_connection.was_executed_with_query, "dummy")
            self.assertEqual(dummy_connection.was_executed_with_filter, "(aab=foo)")

    def test_raw_query_one_or_none_non_existing(self):
        """Test the raw_query_one_or_none method when there is no data."""
        with patch.object(DummyModel.objects, "connect") as mock:
            dummy_connection = DummyConnection([])
            mock.return_value = dummy_connection
            raw_query_result = DummyModel.objects.raw_query_one_or_none("foo")
            self.assertIsNone(raw_query_result)
            self.assertEqual(dummy_connection.was_executed_with_query, "dummy")
            self.assertEqual(dummy_connection.was_executed_with_filter, "(aab=foo)")

    def test_raw_query_all(self):
        """Test the raw_query_all method."""
        with patch.object(DummyModel.objects, "connect") as mock:
            dummy_connection = DummyConnection([("query", {"aab": [b"foo"]})])
            mock.return_value = dummy_connection
            raw_query_result = list(DummyModel.objects.raw_query_all())
            self.assertEqual(len(raw_query_result), 1)
            self.assertEqual(raw_query_result[0], {"aab": [b"foo"]})
            self.assertEqual(dummy_connection.was_executed_with_query, "dummy")
            self.assertEqual(dummy_connection.was_executed_with_filter, None)

    def test_raw_query_all_empty(self):
        """Test the raw_query_all method when there is no data."""
        with patch.object(DummyModel.objects, "connect") as mock:
            dummy_connection = DummyConnection([])
            mock.return_value = dummy_connection
            raw_query_result = list(DummyModel.objects.raw_query_all())
            self.assertEqual(len(raw_query_result), 0)
