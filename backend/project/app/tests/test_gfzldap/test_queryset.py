# SPDX-FileCopyrightText: 2021
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""Tests for the queryset."""

from django.test import TestCase

from ...gfzldap.models import Person
from ...gfzldap.queryset import QuerySet


class TestQuerySet(TestCase):
    """Tests for the QuerySet class."""

    def test_repr(self):
        """Test the repr method."""
        q1 = QuerySet([], {})
        self.assertEqual(repr(q1), "QuerySet([])")
        p1 = Person(uid="aaa")
        p2 = Person(uid="bbb")
        p3 = Person(uid="ccc")
        p4 = Person(uid="ddd")
        p5 = Person(uid="eee")
        q2 = QuerySet([p1, p2, p3], {})
        self.assertEqual(
            repr(q2),
            "QuerySet([Person(uid='aaa'), Person(uid='bbb'), Person(uid='ccc')])",
        )

        q3 = QuerySet([p1, p2, p3, p4], {})
        self.assertEqual(
            repr(q3),
            "QuerySet([Person(uid='aaa'), Person(uid='bbb'), Person(uid='ccc'), ...])",
        )

        q4 = QuerySet([p1, p2, p3, p4, p5], {})
        self.assertEqual(
            repr(q4),
            "QuerySet([Person(uid='aaa'), Person(uid='bbb'), Person(uid='ccc'), ...])",
        )
