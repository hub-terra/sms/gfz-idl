# SPDX-FileCopyrightText: 2022
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""Test classes for the permissions."""

from django.conf import settings
from django.test import TestCase
from django.test.client import RequestFactory
from rest_framework.exceptions import PermissionDenied

from ..permissions import HasIdlToken


class TestHasIdlToken(TestCase):
    """Tests for the HasIdlToken permission class."""

    def test_ok(self):
        """Check that it works with the idl token included."""
        request_factory = RequestFactory()
        request_factory.defaults["HTTP_Authorization"] = f"Bearer {settings.IDL_TOKEN}"
        request = request_factory.get("/")

        check = HasIdlToken()

        try:
            check.raise_exception_if_not_allowed(request)
        except:
            self.fail("There should be no exception")

    def test_exception_header_missing(self):
        """Check that we throw an exception if the header is missing."""
        request_factory = RequestFactory()
        request = request_factory.get("/")

        check = HasIdlToken()

        def run_check():
            check.raise_exception_if_not_allowed(request)

        self.assertRaises(PermissionDenied, run_check)
