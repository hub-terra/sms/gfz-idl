# SPDX-FileCopyrightText: 2022
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""Tests for the hifis related views."""

from unittest.mock import patch

import requests
from django.test import TestCase
from django.urls import reverse

from ...hifis.openidconnect import OpenIdConnectClientException
from ...models import PermissionGroup, UserAccount


class TestSyncGroupsFromOpenIdConnectView(TestCase):
    """Tests for the SyncGroupsFromOpenIdConnectView."""

    def test_add_user_and_groups_to_empty_db(self):
        """
        Test the view with an empty initial database.

        This will create a user and two groups.
        """
        fake_userinfo = {
            "name": "Test User",
            "eduperson_principal_name": "tuser@institute.de",
            "eduperson_entitlement": [
                "urn:geant:helmholtz.de:group:DataHub-GFZ_Testmanagement:"
                + "SensorManagementSystem_dev:datahub.default-member#login-dev.helmholtz.de",
                "urn:geant:helmholtz.de:group:DataHub-GFZ_Testmanagement:"
                + "SensorManagementSystem_dev2:gfz.sms.admin#login-dev.helmholtz.de",
            ],
        }
        self.assertEqual(PermissionGroup.objects.count(), 0)
        self.assertEqual(UserAccount.objects.count(), 0)

        with patch("app.hifis.openidconnect.OpenIdConnectClient.get_userinfo") as mock:
            mock.return_value = fake_userinfo

            with self.settings(
                VO_ADMIN_CONVENTIONS=["*:gfz.sms.admin"],
                VO_MEMBER_CONVENTIONS=["*:datahub.default-member"],
            ):
                response = self.client.get(reverse("sync-groups"))
        self.assertEqual(response.status_code, 204)
        self.assertEqual(UserAccount.objects.count(), 1)
        self.assertEqual(PermissionGroup.objects.count(), 2)

        new_user = UserAccount.objects.get(username="tuser@institute.de")
        new_group1 = PermissionGroup.objects.get(
            name="DataHub-GFZ_Testmanagement:SensorManagementSystem_dev"
        )
        new_group2 = PermissionGroup.objects.get(
            name="DataHub-GFZ_Testmanagement:SensorManagementSystem_dev2"
        )

        self.assertEqual(new_user.username, "tuser@institute.de")
        self.assertEqual(new_user.displayname, "Test User")
        self.assertTrue(new_user.expose)

        self.assertEqual(
            new_group1.name, "DataHub-GFZ_Testmanagement:SensorManagementSystem_dev"
        )
        self.assertEqual(new_group1.description, "")
        self.assertTrue(new_group1.expose)
        self.assertTrue(new_user in new_group1.members.get_queryset())

        self.assertEqual(
            new_group2.name, "DataHub-GFZ_Testmanagement:SensorManagementSystem_dev2"
        )
        self.assertEqual(new_group2.description, "")
        self.assertTrue(new_group2.expose)
        self.assertTrue(new_user in new_group2.admins.get_queryset())

    def test_add_user_and_groups_to_empty_db_for_gfz_groups(self):
        """
        Test the view with an empty initial database for the gfz ldap group.

        This will create a user and two groups.
        One group will come from the ldap (via the IDP), one from the VO.
        Both may have the same name but they should not be mixed.
        """
        fake_userinfo = {
            "name": "Test User",
            "eduperson_principal_name": "tuser@institute.de",
            "eduperson_entitlement": [
                "urn:geant:helmholtz.de:gfz:group:department5."
                + "gfz-sms-member#idp.gfz-potsdam.de",
                "urn:geant:helmholtz.de:group:gfz:department5:"
                + "gfz-sms-admin#login-dev.helmholtz.de",
            ],
        }
        self.assertEqual(PermissionGroup.objects.count(), 0)
        self.assertEqual(UserAccount.objects.count(), 0)

        with patch("app.hifis.openidconnect.OpenIdConnectClient.get_userinfo") as mock:
            mock.return_value = fake_userinfo

            with self.settings(
                VO_ADMIN_CONVENTIONS=["*:gfz-sms-admin"],
                VO_MEMBER_CONVENTIONS=["*:gfz-sms-member"],
            ):
                response = self.client.get(reverse("sync-groups"))
        self.assertEqual(response.status_code, 204)
        self.assertEqual(UserAccount.objects.count(), 1)
        self.assertEqual(PermissionGroup.objects.count(), 2)

        new_user = UserAccount.objects.get(username="tuser@institute.de")
        new_group1 = PermissionGroup.objects.get(
            name="gfz:department5",
            src="urn:geant:helmholtz.de:gfz",
        )
        new_group2 = PermissionGroup.objects.get(
            name="gfz:department5",
            src="urn:geant:helmholtz.de",
        )

        self.assertEqual(new_user.username, "tuser@institute.de")
        self.assertEqual(new_user.displayname, "Test User")
        self.assertTrue(new_user.expose)

        self.assertEqual(new_group1.name, "gfz:department5")
        self.assertEqual(new_group1.src, "urn:geant:helmholtz.de:gfz")
        self.assertEqual(new_group1.description, "")
        self.assertTrue(new_group1.expose)
        self.assertTrue(new_user in new_group1.members.get_queryset())

        self.assertEqual(new_group2.name, "gfz:department5")
        self.assertEqual(new_group2.src, "urn:geant:helmholtz.de")
        self.assertEqual(new_group2.description, "")
        self.assertTrue(new_group2.expose)
        self.assertTrue(new_user in new_group2.admins.get_queryset())

    def test_handle_existing_groups(self):
        """
        Test the sync with existing entries that will be updated.

        We test here that we update the users & the groups
        and the changes in their roles.
        We start with a user that is member&admin in the first group,
        and only member in the second one.

        After the sync we end with member in the first, but only admin
        in the second group.

        """
        fake_userinfo = {
            "name": "Test User",
            "eduperson_principal_name": "tuser@institute.de",
            "eduperson_entitlement": [
                "urn:geant:helmholtz.de:group:DataHub-GFZ_Testmanagement:"
                + "SensorManagementSystem_dev:datahub.default-member#login-dev.helmholtz.de",
                "urn:geant:helmholtz.de:group:DataHub-GFZ_Testmanagement:"
                + "SensorManagementSystem_dev2:gfz.sms.admin#login-dev.helmholtz.de",
            ],
        }
        self.assertEqual(PermissionGroup.objects.count(), 0)
        self.assertEqual(UserAccount.objects.count(), 0)
        existing_user = UserAccount.objects.create(
            username="tuser@institute.de",
            displayname="Some older entry",
            expose=False,
        )
        existing_group2 = PermissionGroup.objects.create(
            name="DataHub-GFZ_Testmanagement:SensorManagementSystem_dev2",
            description="",
            expose=False,
        )
        existing_group2.members.add(existing_user)
        existing_group2.save()

        existing_group1 = PermissionGroup.objects.create(
            name="Some other group",
            description="",
            expose=False,
        )

        existing_group1.members.add(existing_user)
        existing_group1.admins.add(existing_user)
        existing_group1.save()

        with patch("app.hifis.openidconnect.OpenIdConnectClient.get_userinfo") as mock:
            mock.return_value = fake_userinfo

            with self.settings(
                VO_ADMIN_CONVENTIONS=["*:gfz.sms.admin"],
                VO_MEMBER_CONVENTIONS=["*:datahub.default-member"],
            ):
                response = self.client.get(reverse("sync-groups"))
        self.assertEqual(response.status_code, 204)
        self.assertEqual(UserAccount.objects.count(), 1)
        self.assertEqual(PermissionGroup.objects.count(), 3)

        new_user = UserAccount.objects.get(username="tuser@institute.de")
        new_group1 = PermissionGroup.objects.get(
            name="DataHub-GFZ_Testmanagement:SensorManagementSystem_dev"
        )
        existing_group1 = PermissionGroup.objects.get(name="Some other group")
        existing_group2 = PermissionGroup.objects.get(
            name="DataHub-GFZ_Testmanagement:SensorManagementSystem_dev2"
        )

        self.assertEqual(new_user.username, "tuser@institute.de")
        self.assertEqual(new_user.displayname, "Test User")
        self.assertTrue(new_user.expose)

        self.assertEqual(
            new_group1.name, "DataHub-GFZ_Testmanagement:SensorManagementSystem_dev"
        )
        self.assertEqual(new_group1.description, "")
        self.assertTrue(new_group1.expose)
        self.assertTrue(new_user in new_group1.members.get_queryset())

        self.assertEqual(
            existing_group2.name,
            "DataHub-GFZ_Testmanagement:SensorManagementSystem_dev2",
        )
        self.assertEqual(existing_group2.description, "")
        self.assertTrue(existing_group2.expose)
        self.assertTrue(new_user in existing_group2.admins.get_queryset())
        self.assertFalse(new_user in existing_group2.members.get_queryset())

        self.assertEqual(existing_group1.name, "Some other group")
        self.assertEqual(existing_group1.description, "")
        self.assertFalse(existing_group1.expose)
        self.assertFalse(new_user in existing_group1.admins.get_queryset())
        self.assertFalse(new_user in existing_group1.members.get_queryset())

    def test_handle_existing_for_gfz_groups(self):
        """
        Test the view with an existing gfz groups.

        Same as the other test that include a gfz group from the ldap
        (exposed via the idp), this will make sure we don't mix equally
        named groups & their members & admins when they have a different
        source.
        Making sure here means that no one can get access for the ldap
        group when creating a similarly named virtual organization (
        other way around also will not work).
        This is needed for security.
        """
        fake_userinfo = {
            "name": "Test User",
            "eduperson_principal_name": "tuser@institute.de",
            "eduperson_entitlement": [
                "urn:geant:helmholtz.de:gfz:group:department5."
                + "gfz-sms-member#idp.gfz-potsdam.de",
                "urn:geant:helmholtz.de:group:gfz:department5:"
                + "gfz-sms-admin#login-dev.helmholtz.de",
            ],
        }
        existing_group1 = PermissionGroup.objects.create(
            name="gfz:department5",
            src="urn:geant:helmholtz.de:gfz",
            description="",
            expose=False,
        )
        existing_group2 = PermissionGroup.objects.create(
            name="gfz:department5",
            src="urn:geant:helmholtz.de",
            description="",
            expose=False,
        )
        existing_user = UserAccount.objects.create(username="tuser@institute.de")
        existing_group1.admins.add(existing_user)
        existing_group1.save()
        existing_group2.members.add(existing_user)
        existing_group2.save()

        with patch("app.hifis.openidconnect.OpenIdConnectClient.get_userinfo") as mock:
            mock.return_value = fake_userinfo

            with self.settings(
                VO_ADMIN_CONVENTIONS=["*:gfz-sms-admin"],
                VO_MEMBER_CONVENTIONS=["*:gfz-sms-member"],
            ):
                response = self.client.get(reverse("sync-groups"))
        self.assertEqual(response.status_code, 204)
        self.assertEqual(UserAccount.objects.count(), 1)
        self.assertEqual(PermissionGroup.objects.count(), 2)

        reloaded_user = UserAccount.objects.get(username="tuser@institute.de")
        reloaded_group1 = PermissionGroup.objects.get(
            name="gfz:department5",
            src="urn:geant:helmholtz.de:gfz",
        )
        reloaded_group2 = PermissionGroup.objects.get(
            name="gfz:department5",
            src="urn:geant:helmholtz.de",
        )

        self.assertEqual(reloaded_user.username, "tuser@institute.de")
        self.assertEqual(reloaded_user.displayname, "Test User")
        self.assertTrue(reloaded_user.expose)

        self.assertEqual(reloaded_group1.name, "gfz:department5")
        self.assertEqual(reloaded_group1.src, "urn:geant:helmholtz.de:gfz")
        self.assertEqual(reloaded_group1.description, "")
        self.assertTrue(reloaded_group1.expose)
        self.assertTrue(reloaded_user in reloaded_group1.members.get_queryset())
        self.assertFalse(reloaded_user in reloaded_group1.admins.get_queryset())

        self.assertEqual(reloaded_group2.name, "gfz:department5")
        self.assertEqual(reloaded_group2.src, "urn:geant:helmholtz.de")
        self.assertEqual(reloaded_group2.description, "")
        self.assertTrue(reloaded_group2.expose)
        self.assertTrue(reloaded_user in reloaded_group2.admins.get_queryset())
        self.assertFalse(reloaded_user in reloaded_group2.members.get_queryset())

    def test_add_user_to_existing_group_with_users(self):
        """
        Test the sync with some other others and existing group.

        Here we just want to add the new user to the group.
        """
        fake_userinfo = {
            "name": "Test User",
            "eduperson_principal_name": "tuser@institute.de",
            "eduperson_entitlement": [
                "urn:geant:helmholtz.de:group:DataHub-GFZ_Testmanagement:"
                + "SensorManagementSystem_dev:datahub.default-member#login-dev.helmholtz.de",
            ],
        }
        self.assertEqual(PermissionGroup.objects.count(), 0)
        self.assertEqual(UserAccount.objects.count(), 0)
        existing_user1 = UserAccount.objects.create(
            username="olduser@institute.de",
            displayname="Old user",
            expose=True,
        )
        existing_user2 = UserAccount.objects.create(
            username="volduser@institute.de",
            displayname="Very old user",
            expose=True,
        )
        existing_group = PermissionGroup.objects.create(
            name="DataHub-GFZ_Testmanagement:SensorManagementSystem_dev",
            description="",
            expose=True,
        )
        existing_group.members.add(existing_user1)
        existing_group.members.add(existing_user2)
        existing_group.admins.add(existing_user2)
        existing_group.save()

        with patch("app.hifis.openidconnect.OpenIdConnectClient.get_userinfo") as mock:
            mock.return_value = fake_userinfo

            with self.settings(
                VO_ADMIN_CONVENTIONS=["*:gfz.sms.admin"],
                VO_MEMBER_CONVENTIONS=["*:datahub.default-member"],
            ):
                response = self.client.get(reverse("sync-groups"))
        self.assertEqual(response.status_code, 204)
        self.assertEqual(UserAccount.objects.count(), 3)
        self.assertEqual(PermissionGroup.objects.count(), 1)

        new_user = UserAccount.objects.get(username="tuser@institute.de")
        reloaded_group = PermissionGroup.objects.get(
            name="DataHub-GFZ_Testmanagement:SensorManagementSystem_dev"
        )

        self.assertEqual(new_user.username, "tuser@institute.de")
        self.assertEqual(new_user.displayname, "Test User")
        self.assertTrue(new_user.expose)

        self.assertEqual(
            reloaded_group.name, "DataHub-GFZ_Testmanagement:SensorManagementSystem_dev"
        )
        self.assertEqual(reloaded_group.description, "")
        self.assertTrue(reloaded_group.expose)
        self.assertTrue(new_user in reloaded_group.members.get_queryset())
        self.assertTrue(existing_user1 in reloaded_group.members.get_queryset())
        self.assertTrue(existing_user2 in reloaded_group.members.get_queryset())
        self.assertTrue(existing_user2 in reloaded_group.admins.get_queryset())

    def test_sync_with_exception_on_get_userinfo(self):
        """Test that the view returns 401 if we don't provide the access token."""
        with patch("app.hifis.openidconnect.OpenIdConnectClient.get_userinfo") as mock:
            mock.side_effect = OpenIdConnectClientException("no token")

            response = self.client.get(reverse("sync-groups"))
        self.assertEqual(response.status_code, 403)
        self.assertEqual(
            response.json(),
            {
                "detail": "No valid access token to use the idp",
            },
        )

    def test_sync_with_requests_exception_on_get_userinfo(self):
        """Test that the view returns 401 if we have trouble to get the idp config."""
        with patch("app.hifis.openidconnect.OpenIdConnectClient.get_config") as mock:
            mock.side_effect = requests.exceptions.RequestException("no token")

            response = self.client.get(reverse("sync-groups"))
        self.assertEqual(response.status_code, 403)
        self.assertEqual(
            response.json(),
            {
                "detail": "No valid access token to use the idp",
            },
        )

    def test_add_user_with_very_limited_information(self):
        """
        Test the sync with a super reduced userinfo data set.

        In case of github accounts we don't get a lot of informations,
        so we should extract extract as most of them as we can.
        """
        fake_userinfo = {
            "name": "Test User",
            # No eduperson_entitlement array.
            # No eduperson_principal_name.
            # Instead we use the subject, as this is what we always get.
            # And this is also what the SMS uses in case there
            # is no eduperson_principal_name.
            "sub": "123456789",
        }
        self.assertEqual(PermissionGroup.objects.count(), 0)
        self.assertEqual(UserAccount.objects.count(), 0)
        with patch("app.hifis.openidconnect.OpenIdConnectClient.get_userinfo") as mock:
            mock.return_value = fake_userinfo

            with self.settings(
                VO_ADMIN_CONVENTIONS=["*:gfz.sms.admin"],
                VO_MEMBER_CONVENTIONS=["*:datahub.default-member"],
            ):
                response = self.client.get(reverse("sync-groups"))
        self.assertEqual(response.status_code, 204)
        self.assertEqual(UserAccount.objects.count(), 1)
        self.assertEqual(PermissionGroup.objects.count(), 0)

        new_user = UserAccount.objects.get(username="123456789")
        self.assertEqual(new_user.username, "123456789")
        self.assertEqual(new_user.displayname, "Test User")
        self.assertTrue(new_user.expose)
