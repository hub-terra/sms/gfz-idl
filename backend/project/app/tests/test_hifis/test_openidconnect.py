# SPDX-FileCopyrightText: 2022
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""Test classes for the openidconnect module."""

import dataclasses
import typing
from unittest.mock import patch

from django.test import TestCase

from ...hifis.openidconnect import OpenIdConnectClient


@dataclasses.dataclass
class Fake200JsonResponse:
    """Helper class to fake successful requests json responses."""

    data: typing.Dict

    def raise_for_status(self):
        """Don't raise an exception"""
        pass

    def json(self):
        """Return the data."""
        return self.data


class TestOpenIdConnectClient(TestCase):
    """Tests for the OpenIdConnectClient."""

    def test_get_config_ok(self):
        """Test the get_config method without any exceptions.."""
        # The fake data are the result of a call to the helmholtz dev idp.
        fake_data = {
            "authorization_endpoint": "https://login-dev.helmholtz.de/oauth2-as/oauth2-authz",
            "token_endpoint": "https://login-dev.helmholtz.de/oauth2/token",
            "introspection_endpoint": "https://login-dev.helmholtz.de/oauth2/introspect",
            "revocation_endpoint": "https://login-dev.helmholtz.de/oauth2/revoke",
            "issuer": "https://login-dev.helmholtz.de/oauth2",
            "jwks_uri": "https://login-dev.helmholtz.de/oauth2/jwk",
            "scopes_supported": [
                "credentials",
                "openid",
                "profile",
                "display_name",
                "eduperson_entitlement",
                "eduperson_principal_name",
                "offline_access",
                "eduperson_scoped_affiliation",
                "eduperson_unique_id",
                "sn",
                "eduperson_assurance",
                "email",
                "single-logout",
            ],
            "response_types_supported": [
                "code",
                "token",
                "id_token",
                "code id_token",
                "id_token token",
                "code token",
                "code id_token token",
            ],
            "response_modes_supported": ["query", "fragment"],
            "grant_types_supported": ["authorization_code", "implicit"],
            "code_challenge_methods_supported": ["plain", "S256"],
            "request_uri_parameter_supported": True,
            "subject_types_supported": ["public"],
            "userinfo_endpoint": "https://login-dev.helmholtz.de/oauth2/userinfo",
            "id_token_signing_alg_values_supported": ["RS256", "ES256"],
        }
        well_known_url = (
            "https://login-dev.helmholtz.de/oauth2/.well-known/openid-configuration"
        )

        with patch("requests.get") as mock:
            mock.return_value = Fake200JsonResponse(data=fake_data)
            client = OpenIdConnectClient(well_known_url)
            config = client.get_config()
        self.assertEqual(config, fake_data)

    def test_get_userinfo_ok(self):
        """Test the get_userinfo method without any exception."""
        fake_config = {
            "userinfo_endpoint": "https://login-dev.helmholtz.de/oauth2/userinfo",
        }
        fake_data = {
            "eduperson_principal_name": "user@institute.de",
            "eduperson_entitlement": [],
            "name": "Test user",
            "email": "test.user@institute.de",
            "sub": "0000-0000-0000-0000",
        }
        well_known_url = (
            "https://login-dev.helmholtz.de/oauth2/.well-known/openid-configuration"
        )
        client = OpenIdConnectClient(well_known_url)

        # Here we mock 2 things: first the config and second the request to the get userinfo.
        with patch.object(client, "get_config") as mocked_get_config:
            mocked_get_config.return_value = fake_config
            with patch("requests.get") as mocked_requests_get:
                mocked_requests_get.return_value = Fake200JsonResponse(data=fake_data)
                userinfo = client.get_userinfo(authorization_header="")
        self.assertEqual(userinfo, fake_data)
