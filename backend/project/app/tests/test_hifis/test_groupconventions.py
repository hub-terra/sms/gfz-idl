# SPDX-FileCopyrightText: 2022 - 2024
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""Tests for the group conventions."""

import dataclasses
import typing

from django.test import TestCase

from ...hifis.groupconventions import GroupConventions, GroupParser, GroupResult


@dataclasses.dataclass
class GroupParserTestCase:
    """Helper class to handle a list of test cases."""

    group_input: str
    expected_parsed_output: typing.Dict


class TestGroupParser(TestCase):
    """Tests for the GroupParser."""

    def test_parse(self):
        """Test the parse method with various group names."""
        checks = [
            GroupParserTestCase(
                "urn:geant:helmholtz.de:group:DataHub-GFZ_Testmanagement:"
                + "SensorManagementSystem_dev:Member#login-dev.helmholtz.de",
                {
                    "prefix": "urn:geant:helmholtz.de",
                    "servername": "login-dev.helmholtz.de",
                    "vo": "DataHub-GFZ_Testmanagement",
                    "role": "Member",
                    "group": "SensorManagementSystem_dev",
                },
            ),
            GroupParserTestCase(
                "urn:geant:helmholtz.de:group:DataHub-GFZ_Testmanagement:"
                + "SensorManagementSystem_dev:Admin#login-dev.helmholtz.de",
                {
                    "prefix": "urn:geant:helmholtz.de",
                    "servername": "login-dev.helmholtz.de",
                    "vo": "DataHub-GFZ_Testmanagement",
                    "role": "Admin",
                    "group": "SensorManagementSystem_dev",
                },
            ),
            GroupParserTestCase(
                "urn:geant:helmholtz.de:group:Helmholtz-member#"
                + "login-dev.helmholtz.de",
                {
                    "prefix": "urn:geant:helmholtz.de",
                    "servername": "login-dev.helmholtz.de",
                    "vo": "Helmholtz-member",
                    "role": "",
                    "group": "",
                },
            ),
            GroupParserTestCase(
                "urn:geant:helmholtz.de:group:GFZ#login-dev.helmholtz.de",
                {
                    "prefix": "urn:geant:helmholtz.de",
                    "servername": "login-dev.helmholtz.de",
                    "vo": "GFZ",
                    "role": "",
                    "group": "",
                },
            ),
            GroupParserTestCase(
                "urn:geant:helmholtz.de:group:DataHub-GFZ_Testmanagement:"
                + "SensorManagementSystem_dev#login-dev.helmholtz.de",
                {
                    "prefix": "urn:geant:helmholtz.de",
                    "servername": "login-dev.helmholtz.de",
                    "vo": "DataHub-GFZ_Testmanagement",
                    "role": "",
                    "group": "SensorManagementSystem_dev",
                },
            ),
            GroupParserTestCase(
                "urn:geant:helmholtz.de:group:DataHub-GFZ_Testmanagement#"
                + "login-dev.helmholtz.de",
                {
                    "prefix": "urn:geant:helmholtz.de",
                    "servername": "login-dev.helmholtz.de",
                    "vo": "DataHub-GFZ_Testmanagement",
                    "role": "",
                    "group": "",
                },
            ),
            GroupParserTestCase(
                "urn:geant:helmholtz.de:gfz:group:department5.gfz-sms-admin#idp.gfz-potsdam.de",
                {
                    "prefix": "urn:geant:helmholtz.de:gfz",
                    "servername": "idp.gfz-potsdam.de",
                    "vo": "gfz",
                    "role": "gfz-sms-admin",
                    "group": "department5",
                },
            ),
            GroupParserTestCase(
                "urn:geant:helmholtz.de:gfz:group:department5#idp.gfz-potsdam.de",
                {
                    "prefix": "urn:geant:helmholtz.de:gfz",
                    "servername": "idp.gfz-potsdam.de",
                    "vo": "gfz",
                    "role": "",
                    "group": "department5",
                },
            ),
        ]
        for check in checks:
            parsed = GroupParser.parse(check.group_input)
            self.assertEqual(parsed, check.expected_parsed_output)


class TestGroupConventions(TestCase):
    """Tests for the GroupConventions."""

    def setUp(self):
        """Set the test cases up."""
        self.group_conventions = GroupConventions(
            member_conventions=[
                "*:datahub.default-member",
                "*:centre.application-member",
            ],
            admin_conventions=[
                "*:gfz.sms.admin",
            ],
            block_list=[],
        )

    def test_extract_membered_groups_empty_list(self):
        """Test to extract membered groups from an empty list."""
        input_list = []
        membered = self.group_conventions.extract_membered_groups(input_list)
        expected_output = []

        self.assertEqual(membered, expected_output)

    def test_extract_membered_groups_example_list(self):
        """Test to extract membered groups from a list with some data."""
        input_list = [
            "urn:geant:helmholtz.de:group:DataHub-GFZ_Testmanagement:"
            + "SensorManagementSystem_dev#login-dev.helmholtz.de",
            "urn:geant:helmholtz.de:group:DataHub-GFZ_Testmanagement#"
            + "login-dev.helmholtz.de",
            "urn:geant:helmholtz.de:group:DataHub-GFZ_Testmanagement:"
            + "SensorManagementSystem_dev:gfz.sms.admin#login-dev.helmholtz.de",
            "urn:geant:helmholtz.de:group:DataHub-GFZ_Testmanagement:"
            + "SensorManagementSystem_dev:datahub.default-member#login-dev.helmholtz.de",
            "urn:geant:helmholtz.de:group:Helmholtz-member#" + "login-dev.helmholtz.de",
            "urn:geant:helmholtz.de:group:GFZ#login-dev.helmholtz.de",
            "urn:geant:helmholtz.de:group:DataHub-UFZ_Testmanagement:"
            + "SensorManagementSystem_dev:centre.application-member#login-dev.helmholtz.de",
        ]
        membered = self.group_conventions.extract_membered_groups(input_list)
        expected_output = [
            GroupResult(
                name="DataHub-UFZ_Testmanagement:SensorManagementSystem_dev",
                src="urn:geant:helmholtz.de",
            ),
            GroupResult(
                name="DataHub-GFZ_Testmanagement:SensorManagementSystem_dev",
                src="urn:geant:helmholtz.de",
            ),
        ]

        self.assertEqual(set(membered), set(expected_output))

    def test_extract_membered_groups_example_list_with_subgroups(self):
        """
        Test to extract membered groups.

        Use some example group with subgroups.
        """
        input_list = [
            "urn:geant:helmholtz.de:group:DataHub-GFZ_Testmanagement:"
            + "SensorManagementSystem_dev#login-dev.helmholtz.de",
            "urn:geant:helmholtz.de:group:DataHub-GFZ_Testmanagement#"
            + "login-dev.helmholtz.de",
            "urn:geant:helmholtz.de:group:DataHub-GFZ_Testmanagement:"
            + "SensorManagementSystem_dev:gfz.sms.admin#login-dev.helmholtz.de",
            "urn:geant:helmholtz.de:group:Tereno:"
            + "Tereno-NO:Deep-Lake:datahub.default-member#login-dev.helmholtz.de",
            "urn:geant:helmholtz.de:group:Helmholtz-member#" + "login-dev.helmholtz.de",
            "urn:geant:helmholtz.de:group:GFZ#login-dev.helmholtz.de",
        ]
        membered = self.group_conventions.extract_membered_groups(input_list)
        expected_output = [
            GroupResult(
                name="Tereno:Tereno-NO:Deep-Lake", src="urn:geant:helmholtz.de"
            ),
        ]

        self.assertEqual(membered, expected_output)

    def test_extract_membered_groups_example_list_without_member(self):
        """Test to extract membered groups from a list - without a real entry."""
        input_list = [
            "urn:geant:helmholtz.de:group:DataHub-GFZ_Testmanagement:"
            + "SensorManagementSystem_dev#login-dev.helmholtz.de",
            "urn:geant:helmholtz.de:group:DataHub-GFZ_Testmanagement#"
            + "login-dev.helmholtz.de",
            "urn:geant:helmholtz.de:group:DataHub-GFZ_Testmanagement:"
            + "SensorManagementSystem_dev:gfz.sms.admin#login-dev.helmholtz.de",
            "urn:geant:helmholtz.de:group:Helmholtz-member#" + "login-dev.helmholtz.de",
            "urn:geant:helmholtz.de:group:GFZ#login-dev.helmholtz.de",
        ]
        membered = self.group_conventions.extract_membered_groups(input_list)
        expected_output = []

        self.assertEqual(membered, expected_output)

    def test_extract_administrated_groups_empty_list(self):
        """Extract the adminstrated groups with an empty list."""
        input_list = []
        membered = self.group_conventions.extract_administrated_groups(input_list)
        expected_output = []

        self.assertEqual(membered, expected_output)

    def test_extract_administrated_groups_example_list(self):
        """Extract the adminstrated groups from a list with some entries."""
        input_list = [
            "urn:geant:helmholtz.de:group:DataHub-GFZ_Testmanagement:"
            + "SensorManagementSystem_dev#login-dev.helmholtz.de",
            "urn:geant:helmholtz.de:group:DataHub-GFZ_Testmanagement#"
            + "login-dev.helmholtz.de",
            "urn:geant:helmholtz.de:group:DataHub-GFZ_Testmanagement:"
            + "SensorManagementSystem_dev:gfz.sms.admin#login-dev.helmholtz.de",
            "urn:geant:helmholtz.de:group:DataHub-GFZ_Testmanagement:"
            + "SensorManagementSystem_dev:datahub.default-member#login-dev.helmholtz.de",
            "urn:geant:helmholtz.de:group:Helmholtz-member#" + "login-dev.helmholtz.de",
            "urn:geant:helmholtz.de:group:GFZ#login-dev.helmholtz.de",
        ]
        membered = self.group_conventions.extract_administrated_groups(input_list)
        expected_output = [
            GroupResult(
                name="DataHub-GFZ_Testmanagement:SensorManagementSystem_dev",
                src="urn:geant:helmholtz.de",
            )
        ]

        self.assertEqual(membered, expected_output)

    def test_extract_administrated_groups_example_list_blocklist(self):
        """Extract the adminstrated groups applying a block list."""
        group_conventions = GroupConventions(
            member_conventions=["*:default"],
            admin_conventions=["*:gfz.sms.admin"],
            block_list=["DataHub-GFZ_Testmanagement"],
        )
        input_list = [
            "urn:geant:helmholtz.de:group:DataHub-GFZ_Testmanagement:"
            + "SensorManagementSystem_dev#login-dev.helmholtz.de",
            "urn:geant:helmholtz.de:group:DataHub-GFZ_Testmanagement#"
            + "login-dev.helmholtz.de",
            "urn:geant:helmholtz.de:group:DataHub-GFZ_Testmanagement:"
            + "SensorManagementSystem_dev:gfz.sms.admin#login-dev.helmholtz.de",
            "urn:geant:helmholtz.de:group:DataHub-GFZ_Testmanagement:"
            + "SensorManagementSystem_dev:default#login-dev.helmholtz.de",
            "urn:geant:helmholtz.de:group:Helmholtz-member#" + "login-dev.helmholtz.de",
            "urn:geant:helmholtz.de:group:GFZ#login-dev.helmholtz.de",
        ]
        membered = group_conventions.extract_administrated_groups(input_list)
        expected_output = []

        self.assertEqual(membered, expected_output)

    def test_extract_administrated_groups_different_conventions(self):
        """Extract the adminstrated groups applying a different conventions."""
        group_conventions = GroupConventions(
            member_conventions=["*:default"],
            admin_conventions=["moses:datamanager", "*:gfz.sms.admin"],
            block_list=[],
        )
        input_list = [
            "urn:geant:helmholtz.de:group:DataHub-GFZ_Testmanagement:"
            + "SensorManagementSystem_dev#login-dev.helmholtz.de",
            "urn:geant:helmholtz.de:group:DataHub-GFZ_Testmanagement#"
            + "login-dev.helmholtz.de",
            "urn:geant:helmholtz.de:group:DataHub-GFZ_Testmanagement:"
            + "SensorManagementSystem_dev:gfz.sms.admin#login-dev.helmholtz.de",
            "urn:geant:helmholtz.de:group:moses:"
            + "Hydrex2020:datamanager#login-dev.helmholtz.de",
            "urn:geant:helmholtz.de:group:DataHub-GFZ_Testmanagement:"
            + "SensorManagementSystem_dev:default#login-dev.helmholtz.de",
            "urn:geant:helmholtz.de:group:Helmholtz-member#" + "login-dev.helmholtz.de",
            "urn:geant:helmholtz.de:group:GFZ#login-dev.helmholtz.de",
        ]
        membered = group_conventions.extract_administrated_groups(input_list)
        expected_output = [
            GroupResult(name="moses:Hydrex2020", src="urn:geant:helmholtz.de"),
            GroupResult(
                name="DataHub-GFZ_Testmanagement:SensorManagementSystem_dev",
                src="urn:geant:helmholtz.de",
            ),
        ]

        self.assertEqual(len(membered), len(expected_output))
        self.assertEqual(set(membered), set(expected_output))

    def test_extract_membered_groups_without_role_convention(self):
        """Extract the membered groups applying a convention without role."""
        group_conventions = GroupConventions(
            member_conventions=["moses"],
            admin_conventions=["moses:datamanager", "*:gfz.sms.admin"],
            block_list=[],
        )
        input_list = [
            "urn:geant:helmholtz.de:group:DataHub-GFZ_Testmanagement:"
            + "SensorManagementSystem_dev#login-dev.helmholtz.de",
            "urn:geant:helmholtz.de:group:DataHub-GFZ_Testmanagement#"
            + "login-dev.helmholtz.de",
            "urn:geant:helmholtz.de:group:DataHub-GFZ_Testmanagement:"
            + "SensorManagementSystem_dev:gfz.sms.admin#login-dev.helmholtz.de",
            "urn:geant:helmholtz.de:group:moses:"
            + "Hydrex2020:datamanager#login-dev.helmholtz.de",
            "urn:geant:helmholtz.de:group:moses:" + "Hydrex2020#login-dev.helmholtz.de",
            "urn:geant:helmholtz.de:group:moses#login-dev.helmholtz.de",
            "urn:geant:helmholtz.de:group:DataHub-GFZ_Testmanagement:"
            + "SensorManagementSystem_dev:datahub.default-member#login-dev.helmholtz.de",
            "urn:geant:helmholtz.de:group:Helmholtz-member#" + "login-dev.helmholtz.de",
            "urn:geant:helmholtz.de:group:GFZ#login-dev.helmholtz.de",
        ]
        membered = group_conventions.extract_membered_groups(input_list)
        expected_output = [
            GroupResult(name="moses", src="urn:geant:helmholtz.de"),
            GroupResult(name="moses:Hydrex2020", src="urn:geant:helmholtz.de"),
        ]

        self.assertEqual(len(membered), len(expected_output))
        self.assertEqual(set(membered), set(expected_output))

    def test_extract_administrated_groups_example_list_without_admin(self):
        """Extract the adminstated groups from a list - without real entries."""
        input_list = [
            "urn:geant:helmholtz.de:group:DataHub-GFZ_Testmanagement:"
            + "SensorManagementSystem_dev#login-dev.helmholtz.de",
            "urn:geant:helmholtz.de:group:DataHub-GFZ_Testmanagement#"
            + "login-dev.helmholtz.de",
            "urn:geant:helmholtz.de:group:DataHub-GFZ_Testmanagement:"
            + "SensorManagementSystem_dev:datahub.default-member#login-dev.helmholtz.de",
            "urn:geant:helmholtz.de:group:Helmholtz-member#" + "login-dev.helmholtz.de",
            "urn:geant:helmholtz.de:group:GFZ#login-dev.helmholtz.de",
        ]
        membered = self.group_conventions.extract_administrated_groups(input_list)
        expected_output = []

        self.assertEqual(membered, expected_output)

    def test_skip_groups_that_dont_follow_the_structure(self):
        """
        Test that we can skip those cases that don't match an expected structure.

        So that those unexpected cases don't break our extraction.
        """
        input_list = [
            "urn:geant:helmholtz.de:group:Tereno:"
            + "Tereno-NO:Deep-Lake:datahub.default-member#login-dev.helmholtz.de",
            # This here should not give an error.
            "urn:mace:dir:entitlement:common",
        ]
        membered = self.group_conventions.extract_membered_groups(input_list)
        expected_output = [
            GroupResult(name="Tereno:Tereno-NO:Deep-Lake", src="urn:geant:helmholtz.de")
        ]
        self.assertEqual(membered, expected_output)

    def test_extract_membered_groups_gfz_default_groups(self):
        """Extract the membered groups without explicit role."""
        default_gfz_convention = "gfz:*"
        group_conventions = GroupConventions(
            member_conventions=[default_gfz_convention, "*:gfz-sms-member"],
            admin_conventions=["*:gfz-sms-admin"],
            block_list=[],
        )
        input_list = [
            "urn:geant:helmholtz.de:gfz:group:cegit#idp.gfz-potsdam.de",
            "urn:geant:helmholtz.de:group:DataHub-GFZ_Testmanagement:"
            + "SensorManagementSystem_dev#login-dev.helmholtz.de",
            "urn:geant:helmholtz.de:group:DataHub-GFZ_Testmanagement#"
            + "login-dev.helmholtz.de",
            "urn:geant:helmholtz.de:group:DataHub-GFZ_Testmanagement:"
            + "SensorManagementSystem_dev:gfz-sms-admin#login-dev.helmholtz.de",
            "urn:geant:helmholtz.de:group:DataHub-GFZ_Testmanagement:"
            + "SensorManagementSystem_dev:gfz-sms-member#login-dev.helmholtz.de",
            "urn:geant:helmholtz.de:group:Helmholtz-member#" + "login-dev.helmholtz.de",
            "urn:geant:helmholtz.de:group:GFZ#login-dev.helmholtz.de",
            "urn:geant:helmholtz.de:gfz:group:cegit.gfz-sms-admin#idp.gfz-potsdam.de",
        ]
        membered = group_conventions.extract_membered_groups(input_list)
        administrated = group_conventions.extract_administrated_groups(input_list)
        expected_membered = [
            GroupResult(name="gfz:cegit", src="urn:geant:helmholtz.de:gfz"),
            GroupResult(
                name="DataHub-GFZ_Testmanagement:SensorManagementSystem_dev",
                src="urn:geant:helmholtz.de",
            ),
        ]
        expected_administrated = [
            GroupResult(
                name="DataHub-GFZ_Testmanagement:SensorManagementSystem_dev",
                src="urn:geant:helmholtz.de",
            ),
            GroupResult(name="gfz:cegit", src="urn:geant:helmholtz.de:gfz"),
        ]

        self.assertEqual(len(administrated), len(expected_administrated))
        self.assertEqual(set(administrated), set(expected_administrated))
        self.assertEqual(len(membered), len(expected_membered))
        self.assertEqual(set(membered), set(expected_membered))
