# SPDX-FileCopyrightText: 2021
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""Utility classes that work with various collections."""


def kv_csv_to_dict(kv_csv_line):
    """Transform 'x=y,z=abc' strings to dicts."""
    result = {}
    for kv in kv_csv_line.split(","):
        parts = kv.split("=", 1)
        if len(parts) == 2:
            key = parts[0]
            value = parts[1]
            result[key] = value
    return result


def byte_list_to_str_list(byte_list):
    """Transform a list of byte strings to a list of strings."""
    return [x.decode("utf-8") for x in byte_list]
