# SPDX-FileCopyrightText: 2021
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""Utility functions to work with dates."""

import datetime


def parse_YYYYMMDD_to_date(text):
    """Parse a date string to a date."""
    year = int(text[0:4])
    month = int(text[4:6])
    day = int(text[6:8])
    return datetime.date(year=year, month=month, day=day)
