# SPDX-FileCopyrightText: 2021
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""Utility functions & classes to work with classes."""


# From https://stackoverflow.com/questions/5189699/how-to-make-a-class-property
class ClassPropertyDescriptor(object):
    """This is a helper class for the classproperty."""

    def __init__(self, fget, fset=None):
        """Init the object."""
        self.fget = fget
        self.fset = fset

    def __get__(self, obj, klass=None):
        """Get the attribute."""
        if klass is None:
            klass = type(obj)
        return self.fget.__get__(obj, klass)()

    def __set__(self, obj, value):
        """Set the attribute."""
        if not self.fset:
            raise AttributeError("can't set attribute")
        type_ = type(obj)
        return self.fset.__get__(obj, type_)(value)

    def setter(self, func):
        """Set a function."""
        if not isinstance(func, (classmethod, staticmethod)):
            func = classmethod(func)
        self.fset = func
        return self


# Also from
# https://stackoverflow.com/questions/5189699/how-to-make-a-class-property
def classproperty(func):
    """Decorate the method with a classproperty annotation."""
    if not isinstance(func, (classmethod, staticmethod)):
        func = classmethod(func)

    return ClassPropertyDescriptor(func)
