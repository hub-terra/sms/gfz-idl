# SPDX-FileCopyrightText: 2021
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""
Utility functions and classes to work with functions.

Can be seen as an extenion to functools.
"""


class FieldFunctionWrapper:
    """
    Function wrapper for the functions.

    This wrapper here provides a way to
    run functions that are meant to run on
    a field of an object instance.

    It is basically the same as
    field_function(instance.field_name)

    The wrapping process can be necessary
    due to lexical scoping and mutability.
    """

    def __init__(self, field_name, field_function):
        """Init the object."""
        self.field_name = field_name
        self.field_function = field_function

    def __call__(self, instance, *args, **kwargs):
        """Extract the field value and run it with the field function."""
        field_value = getattr(instance, self.field_name)
        return self.field_function(field_value, *args, **kwargs)
