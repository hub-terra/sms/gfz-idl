# SPDX-FileCopyrightText: 2021
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""App config for the gfz idl app."""
from django.apps import AppConfig


class AppConfig(AppConfig):
    """App config for the gfz idl app."""

    default_auto_field = "django.db.models.BigAutoField"
    name = "app"
