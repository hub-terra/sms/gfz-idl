# SPDX-FileCopyrightText: 2022
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""OpenIdConnect related classes."""

import requests
from cachetools import TTLCache, cached


class OpenIdConnectClientException(Exception):
    """Exception in case we can't access the idp."""

    pass


class OpenIdConnectClient:
    """Client to work with the IDP."""

    def __init__(self, well_known_url):
        """
        Init the client with the well known url.

        Well known url is used to query for some
        more configuration (location of the userinfo endpoint).
        """
        self.well_known_url = well_known_url

    @cached(cache=TTLCache(maxsize=1, ttl=600))
    def get_config(self):
        """Get the config from the IDP."""
        config_resp = requests.get(self.well_known_url)
        config_resp.raise_for_status()
        return config_resp.json()

    def get_userinfo(self, authorization_header):
        """
        Run the get userinfo request.

        This will use an auhtorization header:

        'Bearer {access_token}'
        """
        try:
            config = self.get_config()
            userinfo_endpoint = config["userinfo_endpoint"]

            resp_userinfo = requests.get(
                userinfo_endpoint, headers={"Authorization": authorization_header}
            )
            resp_userinfo.raise_for_status()
            return resp_userinfo.json()
        except requests.exceptions.RequestException as e:
            raise OpenIdConnectClientException(e)
