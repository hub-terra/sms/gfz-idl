# SPDX-FileCopyrightText: 2022
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""Hifis specific views."""

from django.conf import settings
from rest_framework.exceptions import PermissionDenied
from rest_framework.response import Response
from rest_framework.views import APIView

from ..models import PermissionGroup, UserAccount
from .groupconventions import GroupConventions
from .openidconnect import OpenIdConnectClient, OpenIdConnectClientException


class SyncGroupsFromOpenIdConnectView(APIView):
    """
    View to sync groups & user from openidconnect userinfo endpoint.

    This special view is not part of the IDL specification and is
    highly specific to work with the Helmholtz (HIFIS) AAI.
    It assumes 3 attributes to given back from the userinfo endpoint:
    - eduperson_principal_name
      This one follows the form of "<username>@<institute>.de"
    - name
      We use this one as the displayname.
    - eduperson_entitlement
      This is a list of groups defined by the virtual organizations of Hifis.
      For example urn:geant:helmholtz.de:group:GFZ#login-dev.helmholtz.de
      In order to work with them in the IDL we need to rely on
      some conventions:
      DataHub:Sub-Project:Role
      while Role is either Admin or Member.
    With those information we can fill the user & groups in our database.
    """

    open_id_connect_client = OpenIdConnectClient(settings.OIDC_WELL_KNOWN_URL)

    def _get_or_create_group_by_name_and_src(self, name, src):
        for group in PermissionGroup.objects.filter(name=name).all():
            if not group.src or group.src == src:
                group.src = src
                return group
        return PermissionGroup.objects.create(name=name, src=src)

    def get(self, request, format=None):
        """
        Run the get request.

        This request takes an header value (Authorization) to extract
        the open id connect access token.
        This token will be send to the IDP get userinfo endpoint.
        With the data we get about the user & the virtual organizations
        that are included, we can create/update our groups & user
        data in this IDL implementation.

        We also make sure that we remove the user from groups that
        are no longer included.

        The groups need to fullfil a naming convention (to handle
        membership & adminstration roles). See groupconventions.py.

        Also Note: All the users & groups that are synced here
        are exposed by default.
        The idea is that we extract the information here as
        they come up to date from the IDP & expose them
        via the IDL from the moment that the user sends this
        request here.

        Only users with are exposed themselves are handled in
        the main logic of the IDL, so we will only show users
        that interacted already with this endpoint here.
        """
        group_conventions = GroupConventions(
            member_conventions=settings.VO_MEMBER_CONVENTIONS,
            admin_conventions=settings.VO_ADMIN_CONVENTIONS,
            block_list=settings.VO_BLOCK_LIST,
        )
        # X-Authorization is added here to make the openapi page work.
        authorization_header = request.headers.get("X-Authorization")
        if not authorization_header:
            authorization_header = request.headers.get("Authorization")
        try:
            userinfo = self.open_id_connect_client.get_userinfo(authorization_header)
        except OpenIdConnectClientException:
            raise PermissionDenied("No valid access token to use the idp")

        username = userinfo.get("eduperson_principal_name")
        if not username:
            # In case we don't have the explicit eduperson_principal_name, then
            # we want to use the sub - as this is the entry that the helmholtz
            # AAI delivers as a primary key.
            # It is just not as readable, but for the work with the IDL it doesn't
            # matter that much.
            username = userinfo["sub"]
        displayname = userinfo["name"]
        userinfo_groups = userinfo.get("eduperson_entitlement", [])
        membered_groups = group_conventions.extract_membered_groups(userinfo_groups)
        administrated_groups = group_conventions.extract_administrated_groups(
            userinfo_groups
        )

        user_account, _created = UserAccount.objects.get_or_create(username=username)
        user_account.displayname = displayname
        user_account.expose = True
        user_account.save()

        for parsed_group in membered_groups:
            group = self._get_or_create_group_by_name_and_src(
                name=parsed_group.name, src=parsed_group.src
            )
            group.description = ""
            group.expose = True
            group.members.add(user_account)
            group.save()
        for parsed_group in administrated_groups:
            group = self._get_or_create_group_by_name_and_src(
                name=parsed_group.name, src=parsed_group.src
            )
            group.description = ""
            group.expose = True
            group.admins.add(user_account)
            group.save()

        for group in PermissionGroup.objects.filter(admins__id=user_account.id):
            remove_from_admins = True
            for parsed_group in administrated_groups:
                if parsed_group.name == group.name:
                    if not group.src or group.src == parsed_group.src:
                        remove_from_admins = False

            if remove_from_admins:
                group.admins.remove(user_account)
                group.save()
        for group in PermissionGroup.objects.filter(members__id=user_account.id):
            remove_from_members = True
            for parsed_group in membered_groups:
                if parsed_group.name == group.name:
                    if not group.src or group.src == parsed_group.src:
                        remove_from_members = False
            if remove_from_members:
                group.members.remove(user_account)
                group.save()

        return Response(status=204)
