# SPDX-FileCopyrightText: 2022 - 2024
# - Nils Brinckmann <nils.brinckmann@gfz-potsdam.de>
# - Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences (GFZ, https://www.gfz-potsdam.de)
#
# SPDX-License-Identifier: EUPL-1.2

"""Conventions to parse & extract group & role information."""

import collections
import fnmatch
import typing


class GroupParser:
    """
    Parser for the group information.

    This parser extracts group & role information from the strings
    that we get for virtual organizations & their subgroups from the
    hifis idp.
    """

    @classmethod
    def parse(cls, content):
        """Parse the stirng to a dict with their group & role information."""
        # For examples take a look in the tests.
        # Our main idea is to destruct the string that we got,
        # and extract the group & the role.
        # If the elements don't contain group or role, they don't
        # follow our conventions of: VO:Group:Role
        result = {}
        result["prefix"], rest_content = cls.parse_prefix(content)
        result["servername"], rest_content = cls.parse_servername(rest_content)
        result["vo"], rest_content = cls.parse_vo(rest_content)
        result["role"], rest_content = cls.parse_role(rest_content)
        result["group"], rest_content = cls.parse_group(rest_content)
        # We need to clean up some of the stuff in order to allow the
        # usage of groups that are defined per centre (ldap groups that
        # are given through to the idp for example).
        # Those are a little bit different from the original ones
        # as all of their information are going to the vo part.
        # (department5.gfz-sms-admin) while the information about
        # the VO is more or less inside the prefix
        # (urn:geant:helmholtz.de:gfz).
        if not result["role"] and not result["group"]:
            vo_splitted = result["vo"].split(".")
            if len(vo_splitted) > 1:
                *group_parts, result["role"] = vo_splitted
                result["group"] = ".".join(group_parts)
                result["vo"] = result["prefix"].split(":")[-1]
            else:
                prefix_splitted = result["prefix"].split(":")
                # In case we have something like
                # urn:geant:helmholtz.de:gfz:group:abc
                # In that case we don't want to use the abc
                # as a vo.
                # Instead we want to use it as group, and try
                # to extract the last part of the prefix as vo.
                # (This is the current way that centre internal
                # groups are given to the IDP).
                if len(prefix_splitted) > 3:
                    vo = prefix_splitted[-1]
                    result["group"] = result["vo"]
                    result["vo"] = vo

        if result["role"] and not result["group"]:
            # If our parser reconized something as a role, but has no group
            # we are going to use that role as group (which is the case
            # in most cases) instead of using it as a role.
            # Role itself at the end is empty
            result["group"], result["role"] = result["role"], result["group"]

        return result

    @staticmethod
    def parse_prefix(content):
        """Split into prefix & rest."""
        if ":group:" in content:
            prefix, rest = content.split(":group:", 1)
            return prefix, rest
        return "", content

    @staticmethod
    def parse_servername(content):
        """Return the servername & the rest."""
        parts = content.split("#")
        servername = parts[-1]
        rest = "#".join(parts[:-1])
        return servername, rest

    @staticmethod
    def parse_vo(content):
        """Return the virtual organization & the rest of the string."""
        if ":" in content:
            vo, rest = content.split(":", 1)
            return vo, rest
        # In case we can't split, all of the name is the vo
        return content, ""

    @staticmethod
    def parse_role(content):
        """Split the role & the rest."""
        parts = content.split(":")
        role = parts[-1]
        rest = ":".join(parts[:-1])
        return role, rest

    @staticmethod
    def parse_group(content):
        """Just return the remaining stuff as group."""
        return content, None


GroupResult = collections.namedtuple("GroupResult", ["name", "src"])


class GroupConventions:
    """Apply our convetions to extract group names for specific roles."""

    def __init__(self, member_conventions, admin_conventions, block_list):
        """
        Init the object with the group names for members & admins.

        Both the member & admin conventions are meant to be lists with
        the following format:

        [
            "VO1:admin",
            "VO2:gfz.sms.delete",
            "*MOSES*:datamanager",
            "*:gfz.sms.admin"
        ]
        so that we have a pattern to match a virtual organization name,
        and a name for the subgroup that should be used to indicate
        the role.

        We hope that we can stay with the same conventions for every VO,
        but maybe we need to make changes for specific ones.

        The VO that is matched first, will be used. The pattern
        follow unix file glob syntax.

        We can also block a VO if it we don't want to handle it at all.
        """
        self.member_conventions = member_conventions
        self.admin_conventions = admin_conventions
        self.block_list = block_list

    def extract_membered_groups(self, userinfo_groups) -> typing.List[GroupResult]:
        """Extract all groups where the user has member role."""
        return list(
            set(self._extract_groups_for_role(userinfo_groups, self.member_conventions))
        )

    def extract_administrated_groups(self, userinfo_groups) -> typing.List[GroupResult]:
        """Extract all groups where the user has admin role."""
        return list(
            set(self._extract_groups_for_role(userinfo_groups, self.admin_conventions))
        )

    def _extract_groups_for_role(self, userinfo_groups, conventions):
        """Extract the group names for a specific role."""
        for userinfo_group in userinfo_groups:
            parts = GroupParser.parse(userinfo_group)
            if parts["vo"] not in self.block_list:
                if self._has_matching_convention(parts, conventions):
                    group_name = parts["vo"]
                    if parts["group"]:
                        group_name += ":" + parts["group"]
                    yield GroupResult(name=group_name, src=parts["prefix"])

    def _has_matching_convention(self, parts, conventions):
        """Check if we have a match with our naming conventions."""
        role = parts.get("role", "")
        vo = parts["vo"]
        for convention in conventions:
            # a convention could look like this
            # "moses:datamanager" or "*:gfz.sms.admin" or "gfz:*"
            if ":" in convention:
                vo_fn_pattern, convention_role = convention.split(":", 1)
            else:
                vo_fn_pattern = convention
                convention_role = None

            if fnmatch.fnmatch(vo, vo_fn_pattern):
                # OK, we matched a VO.
                # If we don't have a convention_role, we
                # allow all of them
                if convention_role is None:
                    return True
                # Otherwise we test if our role that we have is the
                # expected one (convention_role). It so we will go with it.
                # However, if the role is not the convention_role
                # then we don't want to test other conventions anymore.
                # (Otherwise there would be the risk to give admin access
                # in cases that we don't want).
                # We also want to support patterns.
                if fnmatch.fnmatch(role, convention_role):
                    return True
        return False
